#include <stdint.h>
#include <stdio.h>

// !!!!!! single precision !!!!!!
// typedef float real;
// #define H5T_NATIVE_REAL H5T_NATIVE_FLOAT
// #define EPSILON (real)1e-6

// !!!!!! double precision !!!!!!
typedef double real;
#define H5T_NATIVE_REAL H5T_NATIVE_DOUBLE
#define EPSILON (real)1e-12

#undef  M_PI
#define M_PI    (real)3.14159265358979323846

// struct containing all simulation data
// a pointer to this kind of struct can be passed to almost all functions in this project
typedef struct MySystem
{
    real L[3]; // box dimensions
    real l[3]; // grid cell dimensions
    real rd; // range of repulsive MDPD nteraction
    real r_buffer2; // buffer range for neighbor list; larger value leads to more redundant calculations but less frequent calculations of the neighborlist; must be optimized by experiment (I found r_buffer2=0.4^2 good enough);
    real r_nl2; // total range of neighborlist consisting of cut-off range of non-bonded interactions + buffer range
    real b; // repulsive MDPD parameter
    real sigma_wall; // range of wall interaction (wall is located at z=0)
    real gravitational_constant; // gravitational constant n z direction
    real dt; // simulation time step
    real info_dt; // time interval (in seconds) for printing information on simulation progress
    real conf_save_dt_real; // time interval (in simulation time) to save configuration to HDF5 file
    real e_bl; // bonded energy
    real e_bend; // bending energy
    real e_nl; // non-bonded energy including MDP AND LJ
    real e_kin; // kinetic energy
    real e_tot; // total energy
    real e_delta; // change of total energy between two ana intervals
    real p_tot[3]; // total momentum
    real T_kin; // kinetic temperature
    real cl_mean; // mean number of beads within a cell
    real cl_std; // standard deviation for the number of beads within a cell
    real nl_mean; // mean number of neighbors per bead according to neghbor list
    real nl_std; // standard deviation number of neighbors per bead according to neghbor list
    real lc; // mean bond length of polymers
    real kT; // temperature of the thermostat (input value and should coincide with T_kin)
    real pressure; // ideal and virial pressure
    real wall_thickness; // thickness of wall; should be larger than the largest interaction range to avoid interactions through the periodic boundary
    real dz_min_vapor_zone; // minimal thickness of vapor zone
    real z0_vapor_zone; // distance between liquid-vapor-interface and begin of vapor zone
    real surface_tension; // surface tension according to virials
    real angle_mean; // mean value of cos angle between neighboring beads
    real angle_std; // standard deviation value of cos angle between neighboring beads
    real h_crit;  // when h<h_crit, new bulk material is reinserted when evaporating and if reinsertion of bulk is activated
    real h_crit_min; // lower limit for h_crit
    real Lz_bulk; // box z dimension of reinserted bulk material
    real delta_h_reinsertion; // shift of the liquid-vapor-interface due to the reinsertion of bulk material; NOTE: this value is smaller than Lz_bulk because one wall needs to be removed in the process
    real phi0; // volume fraction of non-evaporating beads (e.g. polymers) needed for evaporation with reinsertion
    real delta_phi_crit; //
    real rho_vapor_zone; // target density in vapor phase when evaporating
    real dz_fields; // discretization of fields
    real t_healing; // healing means that total momentum is reset to 0 when reinserting new bulk material while evaporating
    real dt_healing;
    real k_bend; // bending energy constant
    real h0_start; // intrinsic energy difference between alpha and beta phase of pvdf at beginning of simulation
    real h0_end; // intrinsic energy difference between alpha and beta phase of pvdf at end of simulation
    real order_parameter; // measured order parameteri, i.e. density of alpha - density of beta

    double dt_dbl; // time step in double precision needed for output
    double time; // simulation time
    double time_0; // simulation time at the beginning of program
    double sim_time_dbl;

    int n[3]; // number of cells in each dimension
    int n_beads; // number of beads
    int n_cells; // number of cells
    int n_polys; // number of polymers
    int sim_time_int; // number of simulation time steps in total
    int conf_save_dt_int; // interval between savings of configuration to file
    int ana_dt_int; // interval between analyzing observables
    int walltime; // maximum wall clock time for simulation, e.g. 24*3600
    int bl_len_max; // maximum number of bonds for a bead
    int nl_len_max; // maximum number of non-bonded partners for a bead
    int time_step; // current time step
    int n_nl_rebuild; // number of rebuilding neighborlists between ana
	int n_types; // number of particles types
    int vapor_zone_dt_int; // interval for deleting or inserting particle into vapor zone
    int ana_fields_dt_int; // interval between ana of fields
    int n_bins_fields; // number of bins used for the fields
    int sample_fields_dt_int; // interval between sampling fields
    int n_sample_fields; // number of samples to calculate mean
    int peters_dt_int; // interval for peters thermostat
    int integration_scheme; // flag which thermostat to use: 0 (default): DPD; 1: Peters thermostat
    int reinsert_bulk; // flag for reinsertion of bulk material: 1 (default): off; 0: on
    int n_beads_bulk; // number of beads in bulk file
    int n_polys_bulk;// number of polymers in bulk file
    int n_beads_memory; // size of bead arrays in memory; >= n_beads because of evaporation
    int reinsert_bulk_dt_int; // interval to reinsert bulk material
    int n_beads_removed; // number of beads removed during evaporation
    int n_types_non_evaporating; // number of types that expected to not evaporate
    int n_nanos; // number of nanoparticles
    int mc_dt_int; // interval for MC step
    int n_accepts; // number of accepted MC steps
    int n_mc; // number of total MC steps
    int wl_flag; // flag for Wang-Landau algorithm
    int id_orig_rev_size; // size of id_orig_rev array = maximum value of id_orig + 1

    int defragment_memory_dt_int; // interval for defragmentation of memory
    int defragmentation_needed; // flag where defragmentation is needed
    // bead data starting here
    real (* r)[3]; // position
    real (* r_check)[3]; // position after rebuilding nl; needed to check when particles diffused half of r_buffer
    real (* p)[3]; // momentum
    real (* f)[3]; //force
    real * rho; //density
    real * rc; // range of attractive MDPD interaction
    real * gamma; // DPD friction
    real * a; // MDPD attraction
    real * kb; // bond strength
    real * r0; // rest bond length
    real * density_field;
    real (* temperature_field)[3][3];
    real (* pressure_field)[3][3];
    real (* velocity_field)[3];
    real * epsilon_wall_bot; // strength of wall potential bottom
    real * epsilon_wall_top; // strength of wall potential top
    real * epsilon_lj; // LJ strength
    real * sigma_lj; // LJ range

    int * bl; // bond list
    int * bl_len; // number of bonds for a given bead
    int * nl; // neighbor list
    int * nl_len; // number of nonbonded neighbors for a given particle
    int (* cell_neighbor)[27]; // list of cell indices for neighbors of a given cell
    int * poly_offset; // array describes where to find beads of given polymer in memory
    int * poly_N; // number of beads for a given polymer
    int * poly_list; // list of beads within a polymer
    int (* im)[3]; // keeping track of image in which bead is moving
    int (* im_check)[3]; // similar to r_check
    int * id_cell; // cell index for a given bead
    int * type; // particle type
	int * density_z; // total density along z axis needed to calculate h
    int * nano_list; // array describes where to find nanoparticle beads in memory
    int * is_already_in_polymer; // temporary array needed for construction of poly_list

    int * id_orig; // original index or tag of a bead; needed since we sort beads during simulation
    int * id_sort; // temporary array
    int * id_unsort; // for sorting
    int * cell_begin; // for a given cell index, it saves the start of the sorted bead indices
    int * cell_end;
    int * id_orig_rev; // reverse of id_orig, i.e. id_orig_rev[id_orig[i]] = i for all i

    // temporary arrays for sorting
    real (* r_copy)[3];
    real (* p_copy)[3];
    real (* f_copy)[3];
    int * bl_copy;
    int * bl_len_copy;
    int (* im_copy)[3];
    int * id_cell_copy;
    int * id_orig_copy;
    int * type_copy;

    real bead_msd[3];
    real poly_msd[3];
    real re2[3];
    real rg2[3];
	real h;

    char * prefix;
    char * conf_file_name;
    char * ana_file_name;
    char * bulk_file_name;

    FILE * ana_file_pointer;
} MySystem;
