// analyzing all scalar and vector observables and write them to a plain .dat file

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "system.h"
#include "io.h"
#include "mesh.h"
#include "ana.h"
#include "potentials.h"

void ana(MySystem * s)
{
    calc_rho(s);
    ana_e_kin(s);
    ana_e_bl(s);
    ana_e_bend(s);
    ana_e_nl(s);
    ana_e_tot(s);
    ana_p_tot(s);
    ana_T_kin(s);
    ana_cl(s);
    ana_nl(s);
    ana_bead_msd(s);
    ana_poly(s);
    ana_pressure(s);
    ana_h(s);
    ana_surface_tension(s);
    ana_angle(s);
    ana_order_parameter(s);

    fprintf(s->ana_file_pointer, "%.12g %.7g %.7g %.7g %.7g %.7g %.7g %.7g %.7g %.7g %.7g %.7g %.7g %.7g %.7g %.7g %.7g %.7g %.7g %.7g %.7g %.7g %.7g %.7g %.7g %.7g %.7g %.7g %.7g %.7g %.7g %.7g %d %.7g %.7g %.7g %.7g %.7g %.7g %.7g %.7g\n", s->time, s->e_kin, s->e_bl, s->e_bend, s->e_nl, s->e_tot, fabs(s->e_delta) / s->e_tot, s->p_tot[0] / s->n_beads, s->p_tot[1] / s->n_beads, s->p_tot[2] / s->n_beads, s->T_kin, s->cl_mean, s->cl_std, s->nl_mean, s->nl_std, (real)s->ana_dt_int / s->n_nl_rebuild, s->bead_msd[0], s->bead_msd[1], s->bead_msd[2], s->poly_msd[0], s->poly_msd[1], s->poly_msd[2], s->re2[0], s->re2[1], s->re2[2], s->rg2[0], s->rg2[1], s->rg2[2], s->lc, s->pressure, s->h, s->surface_tension, s->n_beads_removed, s->delta_h_reinsertion, s->h_crit, s->L[2], s->angle_mean, s->angle_std, (real)s->n_accepts / s->n_mc, s->order_parameter, s->h0_start + (s->h0_end - s->h0_start) / s->sim_time_int * s->time_step);

    s->n_nl_rebuild = 0;
    s->delta_h_reinsertion = 0.;
    s->n_beads_removed = 0;
    s->n_accepts = 0;
    s->n_mc = 0;
}

// total energy
void ana_e_tot(MySystem * s)
{
    static real e_tot_old = (real)0;
    e_tot_old = s->e_tot;
    s->e_tot = s->e_kin + s->e_bl + s->e_bend + s->e_nl;
    s->e_delta = s->e_tot - e_tot_old;
}

// kinetic energy
void ana_e_kin(MySystem * s)
{
    real sum = (real)0;

    #pragma acc parallel loop present(s[0:1]) reduction(+:sum) collapse(2)
    for(int i = 0; i < s->n_beads; i++)
    {
        for(int d = 0; d < 3; d++)
        {
            const real p = s->p[i][d];
            sum += p * p;
        }
    }
    s->e_kin = (real)0.5 * sum;
}

// bonded energy
void ana_e_bl(MySystem * s)
{
    real sum = (real)0;

    #pragma acc parallel loop present(s[0:1]) reduction(+:sum)
    for(int i = 0; i < s->n_beads; i++)
    {
        #pragma acc loop seq
        for(int j0 = 0; j0 < s->bl_len[i]; j0++)
        {
            int j = s->bl[i * s->bl_len_max + j0];

            if(i < j)
            {
                real r = 0;

                #pragma acc loop seq
                for(int d = 0; d < 3; d++)
                {
                    const real dr = wrap_back_dis(s->r[i][d] - s->r[j][d], s->L[d]);
                    r += dr * dr;
                }

                r = sqrt(r);

                if(r > EPSILON)
                {
                    const int mindex = s->type[i] * s->n_types + s->type[j];
                    const real kb = s->kb[mindex];
                    const real r0 = s->r0[mindex];
                    sum += bond_potential(r, kb, r0);
                }

            }
        }
    }

    s->e_bl = sum;
}

// bending energy
void ana_e_bend(MySystem * s)
{
    real sum = (real)0;

    #pragma acc parallel loop present(s[0:1]) reduction(+:sum)
    for(int i = 0; i < s->n_beads; i++)
    {
        if(s->bl_len[i] == 2 && s->k_bend > EPSILON)
        {
            const int j = s->bl[i * s->bl_len_max + 0];

            real dr_ij[3];
            real r_ij = 0;

            #pragma acc loop seq
            for(int d = 0; d < 3; d++)
            {
                dr_ij[d] = wrap_back_dis(s->r[j][d] - s->r[i][d], s->L[d]);
                r_ij += dr_ij[d] * dr_ij[d];
            }

            r_ij = sqrtf(r_ij);

            const int k = s->bl[i * s->bl_len_max + 1];

            real dr_ik[3];
            real r_ik = 0;

            #pragma acc loop seq
            for(int d = 0; d < 3; d++)
            {
                dr_ik[d] = wrap_back_dis(s->r[k][d] - s->r[i][d], s->L[d]);
                r_ik += dr_ik[d] * dr_ik[d];
            }

            r_ik = sqrtf(r_ik);

            real r_dot_r = 0.;

            #pragma acc loop seq
            for(int d = 0; d < 3; d++)
            {
                r_dot_r += dr_ij[d] * dr_ik[d];
            }

            sum += r_dot_r / (r_ij * r_ik);
        }
    }

    s->e_bend = s->k_bend * sum;
}

// non-bonded energy including MDPD and LJ
void ana_e_nl(MySystem * s)
{
    real sum = (real)0;

    #pragma acc parallel loop present(s[0:1]) reduction(+:sum)
    for(int i = 0; i < s->n_beads; i++)
    {
        #pragma acc loop seq
        for(int j0 = 0; j0 < s->nl_len[i]; j0++)
        {
            int j = s->nl[i * s->nl_len_max + j0];

            if(i < j)
            {
                real r = 0;

                #pragma acc loop seq
                for(int d = 0; d < 3; d++)
                {
                    const real dr = wrap_back_dis(s->r[i][d] - s->r[j][d], s->L[d]);
                    r += dr * dr;
                }

                r = sqrt(r);

                if(r > EPSILON)
                {
                    const int mindex = s->type[i] * s->n_types + s->type[j];

                    const real rc = s->rc[mindex];

                    if(r < rc)
                    {
                        const real a = s->a[mindex];
                        sum += dpd_a_potential(r, a, rc);
                    }
                }
            }
        }

        sum += dpd_b_potential(s->b, s->rd, s->rho[i]);

        #pragma acc loop seq
        for(int j0 = 0; j0 < s->n_nanos; j0++)
        {
            const int j = s->id_orig_rev[s->nano_list[j0]];
            real dr[3];
            real r = 0;

            #pragma acc loop seq
            for(int d = 0; d < 3; d++)
            {
                dr[d] = wrap_back_dis(s->r[i][d] - s->r[j][d], s->L[d]);
                r += dr[d] * dr[d];
            }

            r = sqrtf(r);

            if(r > EPSILON)
            {
                const int mindex = s->type[i] * s->n_types + s->type[j];
                const real sigma = s->sigma_lj[mindex];

                if(r < 2.5 * sigma)
                {
                    const real eps = s->epsilon_lj[mindex];
                    sum += lj_potential(r, eps, sigma);
                }
            }
        }

        const real sigma = s->sigma_wall;
        real dz = s->r[i][2] - (real)0.5 * s->wall_thickness;
        sum += wall_potential(dz, s->epsilon_wall_bot[s->type[i]], sigma);
        dz = s->L[2] - (real)0.5 * s->wall_thickness - s->r[i][2];
        sum += wall_potential(dz, s->epsilon_wall_top[s->type[i]], sigma);
    }

    s->e_nl = sum;
}

// total momentum
void ana_p_tot(MySystem * s)
{
    for(int d = 0; d < 3; d++)
    {
        real sum = (real)0;

        #pragma acc parallel loop present(s[0:1]) reduction(+:sum)
        for(int i = 0; i < s->n_beads; i++)
        {
            sum += s->p[i][d];
        }

        s->p_tot[d] = sum;
    }

    #pragma acc update device(s->p_tot[0:3])
}

// kinetic temperature
void ana_T_kin(MySystem * s)
{
    real sum = (real)0;

    #pragma acc parallel loop present(s[0:1]) reduction(+:sum) collapse(2)
    for(int i = 0; i < s->n_beads; i++)
    {
        for(int d = 0; d < 3; d++)
        {
            const real p = s->p[i][d] - s->p_tot[d] / s->n_beads;
            sum += p * p;
        }
    }

    s->T_kin = sum / (3 * (s->n_beads - 1));
}

// mean and standard deviation for number of beads in a cell
void ana_cl(MySystem * s)
{
    real sum = (real)0;
    real sum2 = (real)0;

    #pragma acc parallel loop present(s[0:1]) reduction(+:sum,sum2)
    for(int c = 0; c < s->n_cells; c++)
    {
        real tmp = s->cell_end[c] - s->cell_begin[c];
        sum += tmp;
        sum2 += tmp * tmp;
    }

    s->cl_mean = sum / s->n_cells;
    s->cl_std = sqrt(sum2 / s->n_cells - s->cl_mean * s->cl_mean);
}

// mean and standard deviation for number of neighbors per bead according to neighborlist
void ana_nl(MySystem * s)
{
    real sum = (real)0;
    real sum2 = (real)0;

    #pragma acc parallel loop present(s[0:1]) reduction(+:sum,sum2)
    for(int i = 0; i < s->n_beads; i++)
    {
        real tmp = s->nl_len[i];
        sum += tmp;
        sum2 += tmp * tmp;
    }

    s->nl_mean = sum / s->n_beads;
    s->nl_std = sqrt(sum2 / s->n_beads - s->nl_mean * s->nl_mean);
}

// mean-square displacement
void ana_bead_msd(MySystem * s)
{
    for(int d = 0; d < 3; d++)
    {
        real sum = (real)0;

        #pragma acc parallel loop present(s[0:1]) reduction(+:sum)
        for(int i = 0; i < s->n_beads; i++)
        {
            real dr = s->r[i][d]+ s->im[i][d] * s->L[d];
            sum += dr * dr;
        }

        s->bead_msd[d] = sum / s->n_beads;
    }
}

// mean-square displacement of center of mass of polymers
void ana_poly_msd(MySystem * s)
{
    for(int d = 0; d < 3; d++)
    {
        real sum = (real)0;

        #pragma acc parallel loop present(s[0:1]) reduction(+:sum)
        for(int p = 0; p < s->n_polys; p++)
        {
            real cm = 0;

            const int offset = s->poly_offset[p];
            const int N = s->poly_N[p];

            #pragma acc loop seq
            for(int i0 = 0; i0 < N; i0++)
            {
                int i = s->poly_list[offset + i0];

                cm += s->r[i][d] + s->im[i][d] * s->L[d];
            }

            real dr = cm / N;

            sum += dr * dr;
        }

        s->poly_msd[d] = sum / s->n_polys;
    }
}

// mean-square end-to-end distance of (linear) polymers
void ana_re2(MySystem * s)
{
    for(int d = 0; d < 3; d++)
    {
        real sum = (real)0;

        #pragma acc parallel loop present(s[0:1]) reduction(+:sum)
        for(int p = 0; p < s->n_polys; p++)
        {
            const int offset = s->poly_offset[p];
            const int N = s->poly_N[p];

            int ind[2];
            int count = 0;

            for(int i0 = 0; i0 < N && count < 2; i0++)
            {
                const int i = s->poly_list[offset + i0];

                if(s->bl_len[i] == 1)
                {
                    ind[count] = i;
                    count++;
                }
            }

            real dr = s->r[ind[0]][d] - s->r[ind[1]][d] + (s->im[ind[0]][d] - s->im[ind[1]][d]) * s->L[d];

            sum += dr * dr;
        }

        s->re2[d] = sum / s->n_polys;
    }
}

// radius of gyration of polymers
void ana_rg2(MySystem * s)
{
    for(int d = 0; d < 3; d++)
    {
        real sum = (real)0;

        #pragma acc parallel loop present(s[0:1]) reduction(+:sum)
        for(int p = 0; p < s->n_polys; p++)
        {
            real cm = (real)0;

            const int offset = s->poly_offset[p];
            const int N = s->poly_N[p];

            #pragma acc loop seq
            for(int i0 = 0; i0 < N; i0++)
            {
                int i = s->poly_list[offset + i0];

                cm += s->r[i][d] + s->im[i][d] * s->L[d];
            }

            cm /= N;

            real dr2 = (real)0;

            #pragma acc loop seq
            for(int i0 = 0; i0 < N; i0++)
            {
                int i = s->poly_list[offset + i0];
                const real dr = s->r[i][d] + s->im[i][d] * s->L[d] - cm;
                dr2 += dr * dr;
            }

            sum += dr2 / N;
        }

        s->rg2[d] = sum / s->n_polys;
    }
}

// mean bond length of polymers
void ana_lc(MySystem * s)
{
    real sum = (real)0;
    int n_bonds = 0;

    #pragma acc parallel loop present(s[0:1]) reduction(+:sum, n_bonds)
    for(int p = 0; p < s->n_polys; p++)
    {
        const int offset = s->poly_offset[p];
        const int N = s->poly_N[p];

        real sum_p = (real)0;
        int n_bonds_p = 0;

        #pragma acc loop seq
        for(int i0 = 0; i0 < N; i0++)
        {
            int i = s->poly_list[offset + i0];

            for(int j0 = 0; j0 < s->bl_len[i]; j0++)
            {
                const int j = s->bl[i * s->bl_len_max + j0];
                real dr2 = 0;

                #pragma acc loop seq
                for(int d = 0; d < 3; d++)
                {
                    const real dr = s->r[i][d] - s->r[j][d] + (s->im[i][d] - s->im[j][d]) * s->L[d];
                    dr2 += dr * dr;
                }

                sum_p += sqrt(dr2);
                n_bonds_p++;
            }
        }

        sum += sum_p;
        n_bonds += n_bonds_p;
    }

    s->lc = sum / n_bonds;
}

// ideal and virial pressure
void ana_pressure(MySystem * s)
{
    real p = (real)0;

    #pragma acc parallel loop present(s[0:1]) reduction(+:p)
    for(int i = 0; i < s->n_beads; i++)
    {
        #pragma acc loop seq
        for(int j0 = 0; j0 < s->bl_len[i]; j0++)
        {
            int j = s->bl[i * s->bl_len_max + j0];
            if(i < j)
            {
                real r = 0;

                #pragma acc loop seq
                for(int d = 0; d < 3; d++)
                {
                    const real dr = wrap_back_dis(s->r[i][d] - s->r[j][d], s->L[d]);
                    r += dr * dr;
                }

                r = sqrt(r);

                if(r > EPSILON)
                {
                    const int mindex = s->type[i] * s->n_types + s->type[j];
                    const real kb = s->kb[mindex];
                    const real r0 = s->r0[mindex];
                    const real f = bond_force(r, kb, r0);

                    p += f * r;
                }
            }
        }

        #pragma acc loop seq
        for(int j0 = 0; j0 < s->nl_len[i]; j0++)
        {
            int j = s->nl[i * s->nl_len_max + j0];
            if(i < j)
            {
                real r = 0;

                #pragma acc loop seq
                for(int d = 0; d < 3; d++)
                {
                    const real dr = wrap_back_dis(s->r[i][d] - s->r[j][d], s->L[d]);
                    r += dr * dr;
                }

                r = sqrt(r);

                if(r > EPSILON)
                {
                    const int mindex = s->type[i] * s->n_types + s->type[j];
                    real f = (real)0;

                    const real rc = s->rc[mindex];

                    if(r < rc)
                    {
                        const real a = s->a[mindex];
                        f += dpd_a_force(r, a, rc);
                    }

                    const real rd = s->rd;

                    if(r < rd)
                    {
                        f += dpd_b_force(r, s->b, rd, s->rho[i], s->rho[j]);
                    }

                    p += f * r;
                }
            }
        }

        const real sigma = s->sigma_wall;

        real dz = s->r[i][2] - (real)0.5 * s->wall_thickness;
        real fz = wall_force(dz, s->epsilon_wall_bot[s->type[i]], sigma);
        real pz = fz * dz;

        dz = s->L[2] - (real)0.5 * s->wall_thickness - s->r[i][2];
        fz = wall_force(dz, s->epsilon_wall_top[s->type[i]], sigma);
        pz += fz * dz;
        pz += s->gravitational_constant * s->r[i][2];
        p += pz;
    }

    s->pressure = (s->n_beads * s->T_kin + p / (real)3) / (s->L[0] * s->L[1] * s->L[2]);
}

// density distribution along z-axis
// needed for the calculation of liquid-vapor-interface position h
void ana_density_z(MySystem * s)
{
    // calculate density histogram in z direction
    #pragma acc parallel loop present(s[0:1])
    for(int z = 0; z < s->n[2]; z++)
    {

        s->density_z[z] = 0;

        #pragma acc loop seq
        for(int xy = 0; xy < s->n[0] * s->n[1]; xy++)
        {
            //const int c = x + s->nx * (y + s->ny * z);
            const int c = xy + s->n[0] * s->n[1] * z;
            s->density_z[z] += s->cell_end[c] - s->cell_begin[c];
        }
    }
}

// calculation of liquid-vapor-interface position h
void ana_h(MySystem * s)
{
    ana_density_z(s);

    // calculate largest position where density is bigger than threshold
    int z0 = (int)(2 * s->sigma_wall * pow((real)2 / (real)5, (real)1 / (real)6) / s->l[2]) + 1;
    int z_max = 0;
    const real rc = sqrt(s->r_nl2);
    const real threshold = (real)1 / (rc * rc * rc);
    const int dz_max = 1;

    #pragma acc parallel loop present(s[0:1]) reduction(max:z_max)
    for(int z = z0; z < s->n[2] - z0; z++)
    {
        real density = (real)0;
        int counter = 0;

        #pragma acc loop seq
        for(int dz = 0; z + dz < s->n[2] - z0 && dz < dz_max; dz++)
        {
            density += s->density_z[z];
            counter++;
        }

        density /= s->L[0] * s->L[1] * s->l[2] * counter;

        if(density > threshold && z > z_max)
        {
            z_max = z;
        }
    }

    s->h = (z_max + (real)0.5) * s->l[2];
}

void ana_surface_tension(MySystem * s)
{
    real pt[3];

    for(int d = 0; d < 3; d++)
    {
        real p = 0;

        #pragma acc parallel loop present(s[0:1]) reduction(+:p)
        for(int i = 0; i < s->n_beads; i++)
        {
            const real dp = s->p[i][d] - s->p_tot[d] / s->n_beads;
            p += dp * dp;

            if(d == 2)
            {
                const real sigma = s->sigma_wall;

                real dz_wall = s->r[i][d] - (real)0.5 * s->wall_thickness;
                real fz = wall_force(dz_wall, s->epsilon_wall_bot[s->type[i]], sigma);
                p += fz * dz_wall;

                dz_wall = s->L[2] - (real)0.5 * s->wall_thickness - s->r[i][d];
                fz = wall_force(dz_wall, s->epsilon_wall_top[s->type[i]], sigma);
                p += fz * dz_wall;
            }

            #pragma acc loop seq
            for(int j0 = 0; j0 < s->nl_len[i]; j0++)
            {
                int j = s->nl[i * s->nl_len_max + j0];

                if(i < j)
                {
                    real dr[3];
                    real r = 0;

                    #pragma acc loop seq
                    for(int d = 0; d < 3; d++)
                    {
                        dr[d] = wrap_back_dis(s->r[i][d] - s->r[j][d], s->L[d]);
                        r += dr[d] * dr[d];
                    }

                    r = sqrt(r);

                    if(r > EPSILON)
                    {
                        const int mindex = s->type[i] * s->n_types + s->type[j];
                        real f = (real)0;

                        const real rc = s->rc[mindex];

                        if(r < rc)
                        {
                            const real a = s->a[mindex];
                            f += dpd_a_force(r, a, rc);
                        }

                        const real rd = s->rd;

                        if(r < rd)
                        {
                            f += dpd_b_force(r, s->b, rd, s->rho[i], s->rho[j]);
                        }

                        p += f * dr[d] * dr[d] / r;
                    }
                }
            }

            #pragma acc loop seq
            for(int j0 = 0; j0 < s->bl_len[i]; j0++)
            {
                int j = s->bl[i * s->bl_len_max + j0];

                if(i < j)
                {
                    real dr[3];
                    real r = 0;

                    #pragma acc loop seq
                    for(int d = 0; d < 3; d++)
                    {
                        dr[d] = wrap_back_dis(s->r[i][d] - s->r[j][d], s->L[d]);
                        r += dr[d] * dr[d];
                    }

                    r = sqrt(r);

                    if(r > EPSILON)
                    {
                        const int mindex = s->type[i] * s->n_types + s->type[j];
                        const real kb = s->kb[mindex];
                        const real r0 = s->r0[mindex];
                        const real f = bond_force(r, kb, r0);

                        p += f * dr[d] * dr[d] / r;
                    }
                }
            }
        }
        pt[d] = p;
    }

    s->surface_tension = (pt[2] - (real)0.5 * (pt[0] + pt[1])) / ((real)2 * s->L[0] * s->L[1]);
}

// mean and standard deviation of cos of angle between neighboring bonds
void ana_angle(MySystem * s)
{
    real sum = (real)0;
    real sum2 = (real)0;
    int n_angles = 0;

    #pragma acc parallel loop present(s[0:1]) reduction(+:sum,sum2,n_angles)
    for(int i = 0; i < s->n_beads; i++)
    {
        if(s->bl_len[i] == 2 && s->k_bend > EPSILON)
        {
            const int j = s->bl[i * s->bl_len_max + 0];

            real dr_ij[3];
            real r_ij = 0;

            #pragma acc loop seq
            for(int d = 0; d < 3; d++)
            {
                dr_ij[d] = wrap_back_dis(s->r[j][d] - s->r[i][d], s->L[d]);
                r_ij += dr_ij[d] * dr_ij[d];
            }

            r_ij = sqrtf(r_ij);

            const int k = s->bl[i * s->bl_len_max + 1];

            real dr_ik[3];
            real r_ik = 0;

            #pragma acc loop seq
            for(int d = 0; d < 3; d++)
            {
                dr_ik[d] = wrap_back_dis(s->r[k][d] - s->r[i][d], s->L[d]);
                r_ik += dr_ik[d] * dr_ik[d];
            }

            r_ik = sqrtf(r_ik);

            real r_dot_r = 0.;

            #pragma acc loop seq
            for(int d = 0; d < 3; d++)
            {
                r_dot_r += dr_ij[d] * dr_ik[d];
            }

            const real tmp = r_dot_r / (r_ij * r_ik);
            sum += tmp;
            sum2 += tmp * tmp;
            n_angles++;
        }
    }

    s->angle_mean = sum / n_angles;
    s->angle_std = sqrt(sum2 / n_angles - s->angle_mean * s->angle_mean);
}

void ana_order_parameter(MySystem * s)
{
    int suma = 0;
    int sumb = 0;

    #pragma acc parallel loop present(s[0:1]) reduction(+:suma,sumb)
    for(int i = 0; i < s->n_beads; i++)
    {
        if(s->type[i] == 0)
        {
            suma++;
        }

        if(s->type[i] == s->n_types - 1)
        {
            sumb++;
        }
    }

    s->order_parameter = (real)(suma - sumb) / (suma + sumb);
}

void ana_poly(MySystem * s)
{
    ana_poly_arch(s);
    ana_poly_msd(s);
    ana_re2(s);
    ana_rg2(s);
    ana_lc(s);
    free_poly(s);
}

void ana_poly_arch(MySystem * s)
{
    s->n_polys = 0;

    s->is_already_in_polymer = malloc(s->n_beads * sizeof *s->is_already_in_polymer);
    s->poly_list = malloc(s->n_beads * sizeof *s->poly_list);
    s->poly_offset = malloc(s->n_beads * sizeof *s->poly_offset);
    s->poly_N = malloc(s->n_beads * sizeof *s->poly_N);

    for(int i = 0; i < s->n_beads; i++)
    {
        s->is_already_in_polymer[i] = 0;
    }

    #pragma acc update host(s->bl_len[0:s->n_beads])
    #pragma acc update host(s->bl[0:s->n_beads * s->bl_len_max])

    for(int i = 0; i < s->n_beads; i++)
    {
        if(!s->is_already_in_polymer[i] && s->bl_len[i] > 0)
        {
            s->is_already_in_polymer[i] = 1;
            s->poly_N[s->n_polys] = 1;

            if(s->n_polys == 0)
            {
                s->poly_offset[s->n_polys] = 0;
            }
            else
            {
                s->poly_offset[s->n_polys] = s->poly_offset[s->n_polys - 1] + s->poly_N[s->n_polys - 1];
            }

            s->poly_list[s->poly_offset[s->n_polys]] = i;
            visit_bl(s, i);
            s->n_polys++;
        }
    }

    #pragma acc update device(s->n_polys)
    #pragma acc enter data copyin(s->poly_list[0:s->n_beads])
    #pragma acc enter data copyin(s->poly_offset[0:s->n_beads])
    #pragma acc enter data copyin(s->poly_N[0:s->n_beads])
}

void visit_bl(MySystem * s, int i)
{
    for(int j0 = 0; j0 < s->bl_len[i]; j0++)
    {
        const int j = s->bl[i * s->bl_len_max + j0];

        if(!s->is_already_in_polymer[j])
        {
            s->is_already_in_polymer[j] = 1;
            s->poly_list[s->poly_offset[s->n_polys] + s->poly_N[s->n_polys]] = j;
            s->poly_N[s->n_polys]++;
            visit_bl(s, j);
        }
    }

    return;
}

void free_poly(MySystem * s)
{
    free(s->is_already_in_polymer);
    #pragma acc exit data delete(s->poly_offset[0:s->n_polys])
    free(s->poly_offset);
    #pragma acc exit data delete(s->poly_N[0:s->n_polys])
    free(s->poly_N);
    #pragma acc exit data delete(s->poly_list[0:s->n_beads])
    free(s->poly_list);
    s->n_polys = 0;
}
