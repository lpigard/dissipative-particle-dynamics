#ifndef SARUPRNG_H
#define SARUPRNG_H
#include <stdint.h>
#include <time.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define LCGC 0x2600e1f7
#define CTpow_LCGA_1 1273716057
#define CTpowseries_LCGA_1 1
#define oWeylOffset 0x8009d14b
#define oWeylPeriod 0xda879add

#undef  M_PI
#define M_PI    (real)3.14159265358979323846
#define TWO_N31 (real)4.65661287307739257812e-10
#define SQRT3 (real)1.7320508075688772935274463

typedef struct Saru_t {
    uint32_t state;
    uint32_t wstate;
} Saru;

#pragma acc routine seq
void saru_init(Saru* self, uint32_t seed1, uint32_t seed2, uint32_t seed3);
#pragma acc routine seq
uint32_t saru_u32(Saru* self);
#pragma acc routine seq
int saru_uniform_int_simple(int n, uint32_t seed1, uint32_t seed2, uint32_t seed3);
#pragma acc routine seq
real saru_uniform(Saru* self);
#pragma acc routine seq
real saru_normal_simple(uint32_t seed1, uint32_t seed2, uint32_t seed3);
#pragma acc routine seq
real saru_uniform_simple(uint32_t seed1, uint32_t seed2, uint32_t seed3);
#pragma acc routine seq
real saru_normal(Saru* self);
#pragma acc routine seq
real saru_normal_simple_alt(uint32_t seed1, uint32_t seed2, uint32_t seed3);

#endif//SARUPRNG_H
