void init_mesh(MySystem *s);
void finalize_mesh(MySystem *s);
#pragma acc routine seq
real wrap_back_dis(const real d, const real l);
#pragma acc routine seq
int id_cell(MySystem const * const s, const int i);
void calc_cell_neighbor(MySystem *s);
