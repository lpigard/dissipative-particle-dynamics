// pseudo-random number generator
// we use saru which is inefficient on on core but very efficient on many cores and gpu. it is also statistically stable.

#include "system.h"
#include "saru.h"

void saru_init(Saru * const self, uint32_t seed1, uint32_t seed2, uint32_t seed3)
{
    seed3 ^= (seed1 << 7) ^ (seed2 >> 6);
    seed2 += (seed1 >> 4) ^ (seed3 >> 15);
    seed1 ^= (seed2 << 9) + (seed3 << 8);
    seed3 ^= 0xA5366B4D * ((seed2 >> 11) ^ (seed1 << 1));
    seed2 += 0x72BE1579 * ((seed1 << 4)  ^ (seed3 >> 16));
    seed1 ^= 0X3F38A6ED * ((seed3 >> 5)  ^ (((int32_t)seed2) >> 22));
    seed2 += seed1 * seed3;
    seed1 += seed3 ^ (seed2 >> 2);
    seed2 ^= ((int32_t)seed2) >> 17;
    self->state  = 0x79dedea3 * (seed1 ^ (((int32_t)seed1) >> 14));
    self->wstate = (self->state + seed2) ^ (((int32_t)self->state) >> 8);
    self->state  = self->state + (self->wstate * (self->wstate ^ 0xdddf97f5));
    self->wstate = 0xABCB96F7 + (self->wstate >> 1);
}

uint32_t saru_u32(Saru * const self)
{
    self->state = CTpow_LCGA_1 * self->state + LCGC * CTpowseries_LCGA_1;
    self->wstate += oWeylOffset + ((((int32_t)self->wstate) >> 31) & oWeylPeriod);
    const uint32_t v = (self->state ^ (self->state >> 26)) + self->wstate;
    return (v ^ (v >> 20)) * 0x6957f5a7;
}

int saru_uniform_int_simple(const int n, uint32_t seed1, uint32_t seed2, const uint32_t seed3)
{
    Saru state;

    if(seed1 > seed2)
    {
        const uint32_t swap = seed1;
        seed1 = seed2;
        seed2 = swap;
    }

    saru_init(&state, seed1, seed2, seed3);
    return (int)(saru_uniform(&state) * n);
}

real saru_uniform(Saru * const self)
{
    return ((real)((int32_t)(saru_u32(self) >> 1)) + (real)0.5) * TWO_N31;
}

real saru_normal_simple(uint32_t seed1, uint32_t seed2, const uint32_t seed3)
{
    Saru state;

    if(seed1 > seed2)
    {
        const uint32_t swap = seed1;
        seed1 = seed2;
        seed2 = swap;
    }

    saru_init(&state, seed1, seed2, seed3);
    return SQRT3 * ((real)2 * saru_uniform(&state) - (real)1);
}

real saru_uniform_simple(uint32_t seed1, uint32_t seed2, const uint32_t seed3)
{
    Saru state;

    if(seed1 > seed2)
    {
        const uint32_t swap = seed1;
        seed1 = seed2;
        seed2 = swap;
    }

    saru_init(&state, seed1, seed2, seed3);
    return saru_uniform(&state);
}

real saru_normal(Saru * const self)
{
    return sqrt((real)-2 * log(saru_uniform(self)))
    * cos((real)2 * M_PI * saru_uniform(self));
}

real saru_normal_simple_alt(uint32_t seed1, uint32_t seed2, const uint32_t seed3)
{
    Saru state;

    if(seed1 > seed2)
    {
        const uint32_t swap = seed1;
        seed1 = seed2;
        seed2 = swap;
    }

    saru_init(&state, seed1, seed2, seed3);
    return saru_normal(&state);
}
