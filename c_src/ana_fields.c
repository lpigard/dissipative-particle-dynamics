// calculates fields (only along z-coordinate) and writes them to file.
// the fields are also a time average over s->ana_fields_dt_int time steps.

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#include "system.h"
#include "io.h"
#include "mesh.h"
#include "ana.h"
#include "ana_fields.h"
#include "potentials.h"

real calc_lambda(int iz, real z1, real z2, MySystem * s)
{
    const int nz = s->n_bins_fields;
    const real dz = s->L[2] / nz;

    if(z1 > z2)
    {
        real zs = z1;
        z1 = z2;
        z2 = zs;
    }

    const int iz1 = (int)(z1 / dz);
    const int iz2 = (int)(z2 / dz);
    real lambda;

    if(iz1 > iz || iz2 < iz || fabs(z2 - z1) < (real)1e-6)
    {
        lambda = (real)0;
    }
    else
    {
        const real norm = (real)1 / (z2 - z1);

        if(iz1 < iz)
        {
            z1 = iz * dz;
        }

        if(iz2 > iz)
        {
            z2 = (iz + 1) * dz;
        }

        lambda = (z2 - z1) * norm;
    }

    return lambda;
}

void ana_pressure_field(MySystem * s)
{
    const int nz = s->n_bins_fields;
    const real dz_layer = s->dz_fields;

    #pragma acc parallel loop present(s[0:1])
    for(int i = 0; i < s->n_beads; i++)
    {
        const int iz = (int)(s->r[i][2] / dz_layer);

        #pragma acc loop seq
        for(int d1 = 0; d1 < 3; d1++)
        {
            #pragma acc loop seq
            for(int d2 = 0; d2 < 3; d2++)
            {
                #pragma acc atomic
                s->pressure_field[iz][d1][d2] += s->p[i][d1] * s->p[i][d2];
            }
        }

        const real sigma = s->sigma_wall;

        real dz_wall = s->r[i][2] - (real)0.5 * s->wall_thickness;
        real fz = wall_force(dz_wall, s->epsilon_wall_bot[s->type[i]], sigma);
        #pragma acc atomic
        s->pressure_field[iz][2][2] += fz * dz_wall;

        dz_wall = s->L[2] - (real)0.5 * s->wall_thickness - s->r[i][2];
        fz = wall_force(dz_wall, s->epsilon_wall_top[s->type[i]], sigma);
        #pragma acc atomic
        s->pressure_field[iz][2][2] += fz * dz_wall;

        #pragma acc loop seq
        for(int j0 = 0; j0 < s->nl_len[i]; j0++)
        {
            int j = s->nl[i * s->nl_len_max + j0];

            if(i < j)
            {
                real dr[3];
                real r = 0;

                #pragma acc loop seq
                for(int d = 0; d < 3; d++)
                {
                    dr[d] = wrap_back_dis(s->r[i][d] - s->r[j][d], s->L[d]);
                    r += dr[d] * dr[d];
                }

                r = sqrt(r);

                if(r > (real)1e-6)
                {
                    const int mindex = s->type[i] * s->n_types + s->type[j];
                    real f = (real)0;

                    const real rc = s->rc[mindex];

                    if(r < rc)
                    {
                        const real a = s->a[mindex];
                        f += dpd_a_force(r, a, rc);
                    }

                    const real rd = s->rd;

                    if(r < rd)
                    {
                        f += dpd_b_force(r, s->b, rd, s->rho[i], s->rho[j]);
                    }

                    const int jz = (int)(s->r[j][2] / dz_layer);
                    if(abs(iz - jz) < nz / 2)
                    {
                        int startz = iz, endz = jz;

                        if(jz < iz)
                        {
                            startz = jz;
                            endz = iz;
                        }

                        #pragma acc loop seq
                        for(int zl = startz; zl <= endz; zl++)
                        {
                            const real lambda = calc_lambda(zl, s->r[i][2], s->r[j][2], s);
                            #pragma acc loop seq
                            for(int d1 = 0; d1 < 3; d1++)
                            {
                                #pragma acc loop seq
                                for(int d2 = 0; d2 < 3; d2++)
                                {
                                    #pragma acc atomic
                                    s->pressure_field[zl][d1][d2] += f * dr[d1] * dr[d2] / r * lambda;
                                }
                            }
                        }
                    }
                }
            }
        }

        #pragma acc loop seq
        for(int j0 = 0; j0 < s->bl_len[i]; j0++)
        {
            int j = s->bl[i * s->bl_len_max + j0];

            if(i < j)
            {
                real dr[3];
                real r = 0;

                #pragma acc loop seq
                for(int d = 0; d < 3; d++)
                {
                    dr[d] = wrap_back_dis(s->r[i][d] - s->r[j][d], s->L[d]);
                    r += dr[d] * dr[d];
                }

                r = sqrt(r);

                if(r > (real)1e-6)
                {
                    const int mindex = s->type[i] * s->n_types + s->type[j];
                    const real kb = s->kb[mindex];
                    const real r0 = s->r0[mindex];
                    const real f = bond_force(r, kb, r0);

                    const int jz = (int)(s->r[j][2] / dz_layer);

                    if(abs(iz - jz) < nz / 2)
                    {
                        int startz = iz, endz = jz;

                        if(jz < iz)
                        {
                            startz = jz;
                            endz = iz;
                        }

                        #pragma acc loop seq
                        for(int zl = startz; zl <= endz; zl++)
                        {
                            const real lambda = calc_lambda(zl, s->r[i][2], s->r[j][2], s);
                            #pragma acc loop seq
                            for(int d1 = 0; d1 < 3; d1++)
                            {
                                #pragma acc loop seq
                                for(int d2 = 0; d2 < 3; d2++)
                                {
                                    #pragma acc atomic
                                    s->pressure_field[zl][d1][d2] += f * dr[d1] * dr[d2] / r * lambda;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

void ana_fields(MySystem * s)
{
    int nz = s->n_bins_fields;
    const real dz = s->dz_fields;
    const int nt = s->n_types;

    ana_h(s);
    calc_rho(s);

    #pragma acc parallel loop present(s[0:1])
    for(int i = 0; i < s->n_beads; i++)
    {
        const int z = (int)(s->r[i][2] / dz);
        const int t = s->type[i];
        const int ind = z * nt + t;
        #pragma acc atomic
        s->density_field[ind] += (real)1;

        #pragma acc loop seq
        for(int d = 0; d < 3; d++)
        {
            const int ind = z * nt + t;
            #pragma acc atomic
            s->velocity_field[ind][d] += s->p[i][d];
        }

        #pragma acc loop seq
        for(int d1 = 0; d1 < 3; d1++)
        {
            #pragma acc loop seq
            for(int d2 = 0; d2 < 3; d2++)
            {
                #pragma acc atomic
                s->temperature_field[z][d1][d2] += s->p[i][d1] * s->p[i][d2];
            }
        }
    }

    ana_pressure_field(s);

    s->n_sample_fields++;
    const int t = s->time_step;

    if(t % s->ana_fields_dt_int == 0 && t > 0)
    {
        char fn[1024];
        sprintf(fn, "%s_ana_fields.h5", s->prefix);
        hid_t file_id;

        if(access(fn, F_OK) == 0)
        {
            file_id = H5Fopen(fn, H5F_ACC_RDWR, H5P_DEFAULT);
        }
        else
        {
            file_id = H5Fcreate(fn, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
        }

        char gn[1024];
        sprintf(gn, "/t%.12g", s->time);
        hid_t group_id = H5Gcreate(file_id, gn, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

        #pragma acc update host(s->density_field[0:nz*nt])
        #pragma acc update host(s->velocity_field[0:nz*nt])
        #pragma acc update host(s->temperature_field[0:nz])
        #pragma acc update host(s->pressure_field[0:nz])

        real * za = malloc(nz * sizeof *za);
        real * zmh = malloc(nz * sizeof *zmh);

        for(int z = 0; z < nz; z++)
        {
            za[z] = (z + (real)0.5) * dz;
            zmh[z] = (z + (real)0.5) * dz - s->h;

            for(int d1 = 0; d1 < 3; d1++)
            {
                for(int d2 = 0; d2 < 3; d2++)
                {
                    s->temperature_field[z][d1][d2] /= s->n_sample_fields;
                    s->pressure_field[z][d1][d2] /= s->n_sample_fields * s->dz_fields * s->L[0] * s->L[1];
                }
            }

            for(int t = 0; t < nt; t++)
            {
                const int ind = z * nt + t;
                s->density_field[ind] /= s->n_sample_fields * s->dz_fields * s->L[0] * s->L[1];

                for(int d = 0; d < 3; d++)
                {
                    const int ind = z * nt + t;
                    s->velocity_field[ind][d] /= s->n_sample_fields;
                }
            }
        }

        hsize_t d1[1] = {nz}, d2[2] = {nz, nt}, d3[3] = {nz, 3, 3}, d4[3] = {nz, nt, 3};

        write_dataset(group_id, za, "z", 1, d1, H5T_NATIVE_REAL);
        write_dataset(group_id, zmh, "z-h", 1, d1, H5T_NATIVE_REAL);
        write_dataset(group_id, s->density_field, "density", 2, d2, H5T_NATIVE_REAL);
        write_dataset(group_id, s->velocity_field, "velocity", 3, d4, H5T_NATIVE_REAL);
        write_dataset(group_id, s->temperature_field, "temperature", 3, d3, H5T_NATIVE_REAL);
        write_dataset(group_id, s->pressure_field, "pressure", 3, d3, H5T_NATIVE_REAL);

        free(za);
        free(zmh);

        H5Gclose(group_id);
        H5Fclose(file_id);

        #pragma acc exit data delete(s->density_field[0:s->n_bins_fields*s->n_types])
        #pragma acc exit data delete(s->temperature_field[0:s->n_bins_fields])
        #pragma acc exit data delete(s->pressure_field[0:s->n_bins_fields])
        #pragma acc exit data delete(s->velocity_field[0:s->n_bins_fields*s->n_types])

        free(s->density_field);
        free(s->temperature_field);
        free(s->pressure_field);
        free(s->velocity_field);

        s->n_bins_fields = (int)(s->L[2] / s->dz_fields);

        s->density_field = malloc(s->n_bins_fields * s->n_types * sizeof *s->density_field);
        s->temperature_field = malloc(s->n_bins_fields * sizeof *s->temperature_field);
        s->pressure_field = malloc(s->n_bins_fields * sizeof *s->pressure_field);
        s->velocity_field = malloc(s->n_bins_fields * s->n_types * sizeof *s->velocity_field);

        #pragma acc enter data copyin(s->density_field[0:s->n_bins_fields*s->n_types])
        #pragma acc enter data copyin(s->temperature_field[0:s->n_bins_fields])
        #pragma acc enter data copyin(s->pressure_field[0:s->n_bins_fields])
        #pragma acc enter data copyin(s->velocity_field[0:s->n_bins_fields*s->n_types])

        #pragma acc parallel loop present(s[0:1])
        for(int z = 0; z < s->n_bins_fields; z++)
        {
            #pragma acc loop seq
            for(int d1 = 0; d1 < 3; d1++)
            {
                #pragma acc loop seq
                for(int d2 = 0; d2 < 3; d2++)
                {
                    s->temperature_field[z][d1][d2] = 0;
                    s->pressure_field[z][d1][d2] = 0;
                }
            }

            #pragma acc loop seq
            for(int t = 0; t < nt; t++)
            {
                const int ind = z * nt + t;
                s->density_field[ind] = 0;

                #pragma acc loop seq
                for(int d = 0; d < 3; d++)
                {
                    const int ind = z * nt + t;
                    s->velocity_field[ind][d] = 0;
                }
            }
        }

        s->n_sample_fields = 0;
    }
}
