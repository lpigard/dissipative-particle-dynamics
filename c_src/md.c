// program starts here

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <assert.h>
#include "system.h"
#include "io.h"
#include "ana_fields.h"
#include <math.h>
#include "ana.h"
#include "potentials.h"
#include "saru.h"
#include "mesh.h"
#include "integrate.h"

real energy_v2(const int i, const int type, MySystem * s)
{
    real energy = 0.;

    #pragma acc parallel loop present(s[0:1]) reduction(+:energy)
    for(int j0 = 0; j0 < s->nl_len[i]; j0++)
    {
        int j = s->nl[i * s->nl_len_max + j0];
        real r = 0;

        #pragma acc loop seq
        for(int d = 0; d < 3; d++)
        {
            const real dr = wrap_back_dis(s->r[i][d] - s->r[j][d], s->L[d]);
            r += dr * dr;
        }

        r = sqrtf(r);

        if(r > EPSILON)
        {
            const int mindex = type * s->n_types + s->type[j];

            const real rc = s->rc[mindex];

            if(r < rc)
            {
                const real a = s->a[mindex];
                energy += dpd_a_potential(r, a, rc);
            }
        }
    }

    return energy;
}

int calc_bin_index(MySystem * s, int n)
{
    int m = 0;
    const int type_alpha = 0;

    #pragma acc parallel loop present(s[0:1]) reduction(+:m)
    for(int i = 0; i < s->n_beads; i++)
    {
        if(s->type[i] == type_alpha)
        {
            m++;
        }
    }

    return m;
}

real calc_flatness(int * h, int n)
{
    real max = log(h[0]);
    real min = max;

    for(int i = 0; i < n; i++)
    {
        const real s = log(h[i]);

        if(s < 2)
        {
            return -1;
        }

        if(s > max)
        {
            max = s;
        }

        if(s < min)
        {
            min = s;
        }
    }

    return min / max;
}

int main(int argc, char *argv[])
{
    struct timespec time_start;
    MySystem sys;
    MySystem * const s = &sys;
    int run_time = 0;

    const int arg_target = 12;

    if(argc - 1 != arg_target)
    {
        printf("USAGE: ./run output-prefix conf-file ana-file-name simulation-time dt conf_save_dt_real rho_vapor_zone gravitational_constant bulk_file_name h0_start h0_end wl_flag(%d / %d)\n", argc - 1, arg_target);
        return -1;
    }

    s->prefix = argv[1];
    s->conf_file_name = argv[2];
    s->ana_file_name = argv[3];
    s->sim_time_dbl = atof(argv[4]);
    s->dt_dbl = atof(argv[5]);
    s->conf_save_dt_real = atof(argv[6]);
    s->rho_vapor_zone = atof(argv[7]);
    s->gravitational_constant = atof(argv[8]);
    s->bulk_file_name = argv[9];
    s->h0_start = atof(argv[10]);
    s->h0_end = atof(argv[11]);
    s->wl_flag = atoi(argv[12]);
    s->nl_len_max = 1;

    initialize_MySystem(s);

    s->time_step = 0;

    clock_gettime(CLOCK_REALTIME, &time_start);

    if(s->wl_flag)
    {
        int n = 1;
        const real h0 = s->h0_start;
        const int type_alpha = 0;
        const int type_beta = s->n_types - 1;
        const real f_final = 1e-6;

        for(int i = 0; i < s->n_beads; i++)
        {
            if(s->type[i] == type_alpha || s->type[i] == type_beta)
            {
                n++;
            }
        }

        real * w = malloc(n * sizeof *w);
        int * h = malloc(n * sizeof *h);
        real f = 1.;
        int ana_counter = 0;

        for(int i = 0; i < n; i++)
        {
            w[i] = 0.;
            h[i] = 0;
        }

        char fn[1024];
        sprintf(fn, "%s_ana_wl_restart.h5", s->prefix);

        if(access(fn, F_OK) == 0)
        {
            hid_t file_id;
            file_id = H5Fopen(fn, H5F_ACC_RDONLY, H5P_DEFAULT);
            read_dataset(file_id, w, "w", H5T_NATIVE_REAL);
            read_dataset(file_id, h, "h", H5T_NATIVE_INT);
            read_dataset(file_id, &f, "f", H5T_NATIVE_REAL);
            H5Fclose(file_id);
        }

        int ind0 = calc_bin_index(s, n);

        for(int iteration = 0; f > f_final; iteration++)
        {
            if(iteration > 0)
            {
                for(int i = 0; i < n; i++)
                {
                    h[i] = 0;
                }
            }

            real flatness = -1.;
            int t = 0;

            for(t = 0; t < n || t < s->n_beads || flatness < 0.9; t++)
            {
                int not_found = 1;
                int i0;

                for(int c = 0; not_found; c++)
                {
                    i0 = saru_uniform_int_simple(s->n_beads, t, c, iteration) % s->n_beads;
                    #pragma acc update host(s->type[i0])
                    if(s->type[i0] == type_alpha || s->type[i0] == type_beta)
                    {
                        not_found = 0;
                    }
                }

                if(not_found)
                {
                    printf("ERROR: No random spin was found to be flipped!\n");
                    return -1;
                }

                const int type0 = s->type[i0];
                real mu;
                int type1;
                int ind1;

                // flip spin
                if(type0 == type_alpha)
                {
                    type1 = type_beta;
                    mu = h0;
                    ind1 = ind0 - 1;
                }
                else
                {
                    type1 = type_alpha;
                    mu = -h0;
                    ind1 = ind0 + 1;
                }

                const real de = energy_v2(i0, type1, s) - energy_v2(i0, type0, s);
                const real p = saru_uniform_simple(t, n, iteration);
                const real p_acc = exp(-(de + mu) / s->kT + w[ind0] - w[ind1]);

                if(p_acc > 1 || p_acc > p)
                {
                    s->type[i0] = type1;
                    #pragma acc update device(s->type[i0])
                    ind0 = ind1;
                }

                h[ind0]++;
                w[ind0] += f;

                flatness = calc_flatness(h, n);

                for(int t = 0; t < 1; t++)
                {
                    integrate(s);
                }

                if(t % 100000 == 0)
                {
                    printf("INFO: iteration=%d, f=%g, t=%d, flatness=%g, h=%d %d %d ... %d %d %d, w=%g %g %g ... %g %g %g, ind0=%d, ind1=%d, s=%g, p_acc=%g, p=%g\n", iteration, f, t, flatness, h[0], h[1], h[2], h[n - 3], h[n - 2], h[n - 1], w[0], w[1], w[2], w[n - 3], w[n - 2], w[n - 1], ind0, ind1, -(de + mu) / s->kT, p_acc, p);

                    if(s->wl_flag == 1 || s->wl_flag == 3)
                    {
                        char fn[1024];
                        sprintf(fn, "%s_ana_wl.h5", s->prefix);
                        hid_t file_id;

                        if(access(fn, F_OK) == 0)
                        {
                            file_id = H5Fopen(fn, H5F_ACC_RDWR, H5P_DEFAULT);
                        }
                        else
                        {
                            file_id = H5Fcreate(fn, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
                        }

                        char gn[1024];
                        sprintf(gn, "/t%d", ana_counter);
                        hid_t group_id = H5Gcreate(file_id, gn, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

                        hsize_t dn[1] = {n}, d1[1] = {1};

                        write_dataset(group_id, w, "w", 1, dn, H5T_NATIVE_REAL);
                        write_dataset(group_id, h, "h", 1, dn, H5T_NATIVE_INT);
                        write_dataset(group_id, &f, "f", 1, d1, H5T_NATIVE_REAL);

                        H5Gclose(group_id);
                        H5Fclose(file_id);
                    }

                    ana_counter++;
                }
            }

            f = 0.5 * f;

            save_conf(s, 1);

            char fn[1024];
            sprintf(fn, "%s_ana_wl_restart.h5", s->prefix);
            hid_t file_id;

            file_id = H5Fcreate(fn, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);

            hsize_t dn[1] = {n}, d1[1] = {1};

            write_dataset(file_id, h, "h", 1, dn, H5T_NATIVE_INT);
            write_dataset(file_id, w, "w", 1, dn, H5T_NATIVE_REAL);
            write_dataset(file_id, &f, "f", 1, d1, H5T_NATIVE_REAL);

            H5Fclose(file_id);
        }

        if(s->wl_flag == 2 && f < f_final)
        {
            char fn[1024];
            sprintf(fn, "%s_ana_wl_final.h5", s->prefix);
            hid_t file_id;

            file_id = H5Fcreate(fn, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);

            char gn[1024];
            sprintf(gn, "/h%.12g", h0);
            hid_t group_id = H5Gcreate(file_id, gn, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

            hsize_t dn[1] = {n}, d1[1] = {1};

            write_dataset(group_id, w, "w", 1, dn, H5T_NATIVE_REAL);
            write_dataset(group_id, &f, "f", 1, d1, H5T_NATIVE_REAL);

            H5Gclose(group_id);
            H5Fclose(file_id);
        }

        run_time = print_info(s, time_start);

        free(w);
        free(h);
    }
    else
    {
        for(int t = 0; t < s->sim_time_int && run_time < s->walltime && NoKeyboardInterrupt; t++)
        {
            write_simulation_data(s);
            defragment_memory(s);
            integrate(s);
            run_time = print_info(s, time_start);
        }
    }

    save_conf(s, 1);
    ana_fields(s);

    printf("runtime = %d\n", run_time);

    finalize_MySystem(s);

    return 0;
}
