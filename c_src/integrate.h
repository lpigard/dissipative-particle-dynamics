void integrate(MySystem * s);
void vv_step_one(MySystem * s);
void vv_step_two(MySystem * s);
void peters_thermostat(MySystem * s);
void mc(MySystem * s);
void calc_nl(MySystem * s);
void calc_forces(MySystem * s);
#pragma acc routine seq
real energy(const int i, MySystem * s);
