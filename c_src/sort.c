// Sorting bead data with respect to cell index.

#include <stdlib.h>
#ifdef _OPENACC
#include <openacc.h>
#include <cuda_runtime.h>
#endif
#include "system.h"
#include "mesh.h"
#include "sort.h"

void sort(int *d_key, int *d_values, int num, void *stream);

void sort_system(MySystem * s)
{
    #pragma acc parallel loop present(s[0:1]) async
    for(int c = 0; c < s->n_cells; c++)
    {
        s->cell_begin[c] = -1;
        s->cell_end[c] = -1;
    }

    #pragma acc parallel loop present(s[0:1]) async
    for(int i = 0; i < s->n_beads; i++)
    {
        s->id_unsort[i] = i;
        s->id_cell[i] = id_cell(s, i);
    }

    #pragma acc wait

    #ifdef _OPENACC

    void *stream = acc_get_cuda_stream(acc_async_sync);

    #pragma acc host_data use_device(s->id_cell, s->id_unsort)
    {
        sort(s->id_cell, s->id_unsort, s->n_beads, stream);
    }

    #else

    Sort_item * item = (Sort_item*)malloc(s->n_beads * sizeof *item);

    for(int i = 0; i < s->n_beads; i++)
    {
        item[i].id_cell = s->id_cell[i];
        item[i].id_unsort = s->id_unsort[i];
    }

    qsort(item, s->n_beads, sizeof(Sort_item), compare);

    for(int i = 0; i < s->n_beads; i++)
    {
        s->id_cell[i] = item[i].id_cell;
        s->id_unsort[i] = item[i].id_unsort;
    }

    free(item);

    #endif

    #pragma acc parallel loop present(s[0:1])
    for(int i = 0; i < s->n_beads; i++)
    {
        const int j = s->id_unsort[i];
        s->id_sort[j] = i;
        s->id_orig_copy[i] = s->id_orig[j];

        #pragma acc loop seq
        for(int d = 0; d < 3; d++)
        {
            s->r_copy[i][d] = s->r[j][d];
            s->p_copy[i][d] = s->p[j][d];
            s->f_copy[i][d] = s->f[j][d];
            s->im_copy[i][d] = s->im[j][d];
        }

        s->bl_len_copy[i] = s->bl_len[j];
        s->type_copy[i] = s->type[j];
    }

    #pragma acc parallel loop present(s[0:1])
    for(int i = 0; i < s->n_beads; i++)
    {
        const int j = s->id_unsort[i];

        #pragma acc loop seq
        for(int b0 = 0; b0 < s->bl_len[j]; b0++)
        {
            const int b_unsort = s->bl[j * s->bl_len_max + b0];
            const int b_sort = s->id_sort[b_unsort];
            s->bl_copy[i * s->bl_len_max + b0] = b_sort;
        }
    }

    #pragma acc parallel loop present(s[0:1])
    for(int i = 0; i < s->n_beads; i++)
    {
        s->id_orig[i] = s->id_orig_copy[i];
        const int j = s->id_orig[i];
        s->id_orig_rev[j] = i;

        #pragma acc loop seq
        for(int d = 0; d < 3; d++)
        {
            s->r[i][d] = s->r_copy[i][d];
            s->p[i][d] = s->p_copy[i][d];
            s->f[i][d] = s->f_copy[i][d];
            s->im[i][d] = s->im_copy[i][d];
            s->r_check[i][d] = s->r[i][d];
            s->im_check[i][d] = s->im[i][d];
        }

        s->bl_len[i] = s->bl_len_copy[i];
        s->type[i] = s->type_copy[i];

        const int c1 = s->id_cell[i];
        const int i2 = i + 1 < s->n_beads ? i + 1 : 0;
        const int c2 = s->id_cell[i2];

        if(c1 != c2)
        {
            s->cell_begin[c2] = i2;
            s->cell_end[c1] = i2;
        }

        if(i == s->n_beads - 1)
        {
            s->cell_end[c1] = s->n_beads;
        }

        #pragma acc loop seq
        for(int b0 = 0; b0 < s->bl_len[i]; b0++)
        {
            s->bl[i * s->bl_len_max + b0] = s->bl_copy[i * s->bl_len_max + b0];
        }
    }
}

int compare(const void *s1, const void *s2)
{
  Sort_item *e1 = (Sort_item *)s1;
  Sort_item *e2 = (Sort_item *)s2;
  return e1->id_cell - e2->id_cell;
}
