#pragma acc routine seq
real calc_lambda(int iz, real z1, real z2, MySystem * s);
void ana_fields(MySystem * s);
void ana_pressure_field_gpu(MySystem * s);
