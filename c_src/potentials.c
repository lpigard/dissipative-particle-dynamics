// describing all physical potentials and their derived forces

#include <math.h>
#include "system.h"
#include "mesh.h"
#include "potentials.h"

real bond_potential(const real r, const real k, const real r0)
{
    const real dr = r - r0;
    return (real)0.5 * k * dr * dr;
}

real bond_force(const real r, const real k, const real r0)
{
    return -(real)k * (r - r0);
}

real dpd_a_potential(const real r, const real a, const real rc)
{
    real dr = (real)1 - r / rc;
    return (real)0.5 * a * rc * dr * dr;
}

real dpd_a_force(const real r, const real a, const real rc)
{
	return a * ((real)1 - r / rc);
}

void calc_rho(MySystem * s)
{
    #pragma acc parallel loop present(s[0:1])
    for(int i = 0; i < s->n_beads; i++)
    {
        real rho = (real)0;
        const real rd = s->rd;

        #pragma acc loop seq
        for(int j0 = 0; j0 < s->nl_len[i]; j0++)
        {
            const int j = s->nl[i * s->nl_len_max + j0];
            real r = 0;

            #pragma acc loop seq
            for(int d = 0; d < 3; d++)
            {
                const real dr = wrap_back_dis(s->r[i][d] - s->r[j][d], s->L[d]);
                r += dr * dr;
            }

            r = sqrt(r);

            if(r < rd && r > EPSILON)
            {
                const real w = (real)1 - r / rd;
                rho += w * w;
            }
        }

        s->rho[i] = rho * (real)15 / ((real)2 * M_PI * rd * rd * rd);
    }
}

real dpd_b_potential(const real b, const real rd, const real rho)
{
    const real rd2 = rd * rd;
    return M_PI * rd2 * rd2 / (real)30 * b * rho * rho;
}

real dpd_b_force(const real r, const real b, const real rd, const real rho_i, const real rho_j)
{
    return b * (rho_i + rho_j) * ((real)1 - r / rd);
}

real lj_potential(const real r, const real epsilon, const real sigma)
{
    real r6inv = sigma / r;
    r6inv *= r6inv * r6inv;
    r6inv *= r6inv;
    return 4 * epsilon * (r6inv * r6inv - r6inv + 0.004079222784f);
}

real lj_force(const real r, const real epsilon, const real sigma)
{
    real r6inv = sigma / r;
    r6inv *= r6inv * r6inv;
    r6inv *= r6inv;
    return 4 * epsilon * (12 * r6inv * r6inv - 6 * r6inv) / r;
}

real wall_potential(const real z, const real epsilon, const real sigma)
{
    if(epsilon > EPSILON)
    {
        real r3inv = sigma / z;
        r3inv *= r3inv * r3inv;
        return (real)4 * epsilon * ((real)2 / (real)15 * r3inv * r3inv * r3inv - r3inv);
    }
    else if(epsilon < -EPSILON)
    {
        real r3inv = sigma / z;
        r3inv *= r3inv * r3inv;
        return -(real)4 * epsilon * ((real)2 / (real)15 * r3inv * r3inv * r3inv);
    }
    else
    {
        return (real)0;
    }
}

real wall_force(const real z, const real epsilon, const real sigma)
{
    if(epsilon > EPSILON)
    {
        real r3inv = sigma / z;
        r3inv *= r3inv * r3inv;
        return (real)4 * epsilon * ((real)6 / (real)5 * r3inv * r3inv * r3inv - (real)3 * r3inv) / z;
    }
    else if(epsilon < -EPSILON)
    {
        real r3inv = sigma / z;
        r3inv *= r3inv * r3inv;
        return -(real)4 * epsilon * ((real)6 / (real)5 * r3inv * r3inv * r3inv) / z;
    }
    else
    {
        return (real)0;
    }
}
