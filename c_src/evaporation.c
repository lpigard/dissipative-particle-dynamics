// handling the evaporation and reinsertion of new bulk material (if used).

#include <stdlib.h>
#include <math.h>
#include "system.h"
#include "evaporation.h"
#include "stdio.h"
#include "ana.h"
#include "mesh.h"
#include "io.h"
#include "saru.h"

int set_vapor_density(MySystem * s)
{
    const int n_beads_old = s->n_beads;
    int dn = 0;

    ana_h(s);
    const real z0 = s->h + s->z0_vapor_zone;
    int c0 = (int)(floor(z0 / s->l[2]) * s->n[0] * s->n[1]);

    if(c0 >= 0 && c0 < s->n_cells)
    {
        int n_vapor = 0;

        #pragma acc parallel loop present(s[0:1]) reduction(+:n_vapor)
        for(int c = c0; c < s->n_cells; c++)
        {
            n_vapor = s->cell_end[c] - s->cell_begin[c];
        }

        const real zmax = s->L[2] - 2. * s->sigma_wall;
        const int n_vapor_target = s->rho_vapor_zone * s->L[0] * s->L[1] * (zmax - z0);

        if(s->time_step % 2 == 0 && n_vapor > 0)
        {
            const real p = saru_uniform_simple(n_beads_old, n_beads_old, s->time_step);
            const real p_acc = (real)n_vapor / n_vapor_target;

            if(p_acc > 1 || p_acc > p || n_vapor_target == 0)
            {
                const int i0 = s->n_beads - n_vapor + saru_uniform_int_simple(n_vapor, n_beads_old, n_beads_old + 1, s->time_step) % n_vapor;
                delete_bead(s, i0);
                s->n_beads -= 1;
                #pragma acc update device(s->n_beads)
            }
        }

        if(s->time_step % 2 == 1)
        {
            const real p = saru_uniform_simple(n_beads_old, n_beads_old, s->time_step);
            const real p_acc = (real)n_vapor_target / (n_vapor + 1);

            if(p_acc > 1 || p_acc > p)
            {
                const int n_beads_new = s->n_beads + 1;

                if(n_beads_new > s->n_beads_memory)
                {
                    reallocate_bead_data(s, n_beads_new);
                }

                s->n_beads = n_beads_new;
                #pragma acc update device(s->n_beads)

                #pragma acc parallel loop present(s[0:1])
                for(int i = n_beads_old; i < s->n_beads; i++)
                {
                    s->bl_len[i] = 0;

                    s->r[i][0] = s->L[0] * saru_uniform_simple(i, i + n_beads_new + 0, s->time_step);
                    s->r[i][1] = s->L[1] * saru_uniform_simple(i, i + n_beads_new + 1, s->time_step);
                    s->r[i][2] = z0 + (zmax - z0) * saru_uniform_simple(i, i + n_beads_new + 2, s->time_step);

                    #pragma acc loop seq
                    for(int d = 0; d < 3; d++)
                    {
                        const real xi = saru_normal_simple(i, i + n_beads_new + 3 + d, s->time_step);
                        s->p[i][d] = sqrtf((real)2 * s->kT) * xi;
                        s->im[i][d] = 0;
                    }

                    s->type[i] = 1;
                }

                assign_new_tags(s, n_beads_old, s->n_beads);
            }
        }

        dn = s->n_beads - n_beads_old;

        if(dn != 0)
        {
            ana_p_tot(s);

            #pragma acc parallel loop present(s[0:1])
            for(int i = 0; i < s->n_beads; i++)
            {
                s->p[i][0] -= s->p_tot[0] / s->n_beads;
                s->p[i][1] -= s->p_tot[1] / s->n_beads;
                s->p[i][2] -= s->p_tot[2] / s->n_beads;
            }
        }

        s->n_beads_removed += dn;
    }

    return dn;
}

void reinsert_bulk(MySystem * s)
{
    #pragma acc update host(s->r[0:s->n_beads_memory])
    #pragma acc update host(s->p[0:s->n_beads_memory])
    #pragma acc update host(s->im[0:s->n_beads_memory])
    #pragma acc update host(s->bl[0:s->n_beads_memory*s->bl_len_max])
    #pragma acc update host(s->bl_len[0:s->n_beads_memory])
    #pragma acc update host(s->type[0:s->n_beads_memory])
    #pragma acc update host(s->id_orig[0:s->n_beads_memory])

    const real z_shift = s->Lz_bulk - s->wall_thickness;
    s->delta_h_reinsertion += z_shift;

    for(int i = 0; i < s->n_beads; i++)
    {
        s->r[i][2] += z_shift;
    }

    ana_h(s);
    s->h += z_shift;
    s->L[2] = s->h + s->z0_vapor_zone + s->dz_min_vapor_zone + 2. * s->sigma_wall;
    int imax = -1;

    for(int i = 0; i < s->n_beads; i++)
    {
        if(s->r[i][2] < s->L[2] - 2. * s->sigma_wall && i > imax)
        {
            imax = i;
        }
    }

    const int n_beads_old = imax + 1; // delete upper part of vapor zone for optimization
    const int n_beads_new = n_beads_old + s->n_beads_bulk;

    reallocate_bead_data(s, n_beads_new);

    s->n_beads = n_beads_new;
    #pragma acc update device(s->n_beads)

    hid_t file_id = H5Fopen(s->bulk_file_name, H5F_ACC_RDONLY, H5P_DEFAULT);

    read_dataset(file_id, s->r + n_beads_old * 3, "r", H5T_NATIVE_REAL);
    read_dataset(file_id, s->im + n_beads_old * 3, "im", H5T_NATIVE_INT);
    read_dataset(file_id, s->p + n_beads_old * 3, "p", H5T_NATIVE_REAL);
    read_dataset(file_id, s->type + n_beads_old, "type", H5T_NATIVE_INT);
    read_dataset(file_id, s->bl + n_beads_old * s->bl_len_max, "bl", H5T_NATIVE_INT);
    read_dataset(file_id, s->bl_len + n_beads_old, "bl_len", H5T_NATIVE_INT);
    read_dataset(file_id, s->id_orig + n_beads_old, "id_orig", H5T_NATIVE_INT);

    H5Fclose(file_id);

    for(int i = n_beads_old; i < s->n_beads; i++)
    {

        for(int b0 = 0; b0 < s->bl_len[i]; b0++)
        {
            s->bl[i * s->bl_len_max + b0] += n_beads_old;
        }
    }

    assign_new_tags(s, n_beads_old, s->n_beads);

    finalize_mesh(s);
    init_mesh(s);
    #pragma acc update device(s->L[2:1])
    #pragma acc update device(s->n[2:1])
    #pragma acc update device(s->l[2:1])
    #pragma acc update device(s->n_cells)

    #pragma acc enter data copyin(s->cell_begin[0:s->n_cells])
    #pragma acc enter data copyin(s->cell_end[0:s->n_cells])
    #pragma acc enter data copyin(s->cell_neighbor[0:s->n_cells][0:27])
    #pragma acc enter data copyin(s->density_z[0:s->n[2]])

    #pragma acc update device(s->r[0:s->n_beads])
    #pragma acc update device(s->p[0:s->n_beads])
    #pragma acc update device(s->im[0:s->n_beads])
    #pragma acc update device(s->type[0:s->n_beads])
    #pragma acc update device(s->id_orig[0:s->n_beads])
    #pragma acc update device(s->bl[0:s->n_beads*s->bl_len_max])
    #pragma acc update device(s->bl_len[0:s->n_beads])

    // remove total momentum from particles -> only possible in x,y direction because of Galilean invariance
    ana_p_tot(s);

    #pragma acc parallel loop present(s[0:1])
    for(int i = 0; i < s->n_beads; i++)
    {
        for(int d = 0; d < 3; d++)
        {
            s->p[i][d] -= s->p_tot[d] / s->n_beads;
        }
    }

    s->t_healing = s->time;
}

void calc_h_crit(MySystem * s)
{
    const real dz = s->dz_fields;
    const int nt = s->n_types;
    real phi_max = 0.;
    real z_max = 0.;
    const int z0 = (int)((0.5 * s->wall_thickness + 3. * s->sigma_wall) / dz);
    int z1 = (int)((s->L[2] - (0.5 * s->wall_thickness + 3. * s->sigma_wall)) / dz);

    if(z1 >= s->n_bins_fields)
    {
        z1 = s->n_bins_fields;
    }

    for(int z = z0; z < z1; z++)
    {
        real density = 0.;
        real density_non_evaporating = 0.;

        for(int t = 0; t < nt; t++)
        {
            const real d = s->density_field[z * nt + t];
            density += d;

            if(t < s->n_types_non_evaporating)
            {
                density_non_evaporating += d;
            }
        }

        const real phi = density_non_evaporating / density;

        if(phi > phi_max && density > 2.)
        {
            phi_max = phi;
            z_max = z * dz;
        }
    }

    if((phi_max - s->phi0) > s->delta_phi_crit)
    {
        const real phi_half = 0.5 * (s->phi0 + phi_max);
        real z_half = 0.;

        for(int z = z0; z < z1; z++)
        {
            real density = 0.;
            real density_non_evaporating = 0.;

            for(int t = 0; t < nt; t++)
            {
                const real d = s->density_field[z * nt + t];
                density += d;

                if(t < s->n_types_non_evaporating)
                {
                    density_non_evaporating += d;
                }
            }

            const real phi = density_non_evaporating / density;

            if(phi > phi_half && density > 2.)
            {
                z_half = (z - 1) * dz;
                break;
            }
        }

        const real dz1 = s->h - z_max + 6 * (z_max - z_half); // if density is decaying exponentially, then the density decayed to 1.6% of maximum at 6 * z_half
        const real dz2 = s->h_crit_min;

        if(dz1 > dz2)
        {
            s->h_crit = dz1;
        }
        else
        {
            s->h_crit = dz2;
        }
    }
}

void healing(MySystem * s)
{
    if((s->time - s->t_healing < s->dt_healing && s->reinsert_bulk) || s->gravitational_constant > 1e-12)
    {
        ana_p_tot(s);

        #pragma acc parallel loop present(s[0:1])
        for(int i = 0; i < s->n_beads; i++)
        {
            #pragma acc loop seq
            for(int d = 0; d < 3; d++)
            {
                s->p[i][d] -= s->p_tot[d] / s->n_beads;
            }
        }
    }
}

int reallocate_bead_data(MySystem * s, const int n_beads_new)
{
    if(n_beads_new > s->n_beads_memory) // reallocate all bead arrays
    {
        #pragma acc parallel loop present(s[0:1])
        for(int i = 0; i < s->n_beads_memory; i++)
        {
            s->id_orig_copy[i] = s->id_orig[i];

            #pragma acc loop seq
            for(int d = 0; d < 3; d++)
            {
                s->r_copy[i][d] = s->r[i][d];
                s->p_copy[i][d] = s->p[i][d];
                s->im_copy[i][d] = s->im[i][d];
            }

            s->bl_len_copy[i] = s->bl_len[i];
            s->type_copy[i] = s->type[i];

            #pragma acc loop seq
            for(int b0 = 0; b0 < s->bl_len[i]; b0++)
            {
                s->bl_copy[i * s->bl_len_max + b0] = s->bl[i * s->bl_len_max + b0];
            }
        }

        #pragma acc exit data delete(s->r[0:s->n_beads_memory])
        #pragma acc exit data delete(s->r_check[0:s->n_beads_memory])
        #pragma acc exit data delete(s->p[0:s->n_beads_memory])
        #pragma acc exit data delete(s->f[0:s->n_beads_memory])
        #pragma acc exit data delete(s->rho[0:s->n_beads_memory])
        #pragma acc exit data delete(s->im[0:s->n_beads_memory])
        #pragma acc exit data delete(s->im_check[0:s->n_beads_memory])
        #pragma acc exit data delete(s->bl[0:s->n_beads_memory*s->bl_len_max])
        #pragma acc exit data delete(s->bl_len[0:s->n_beads_memory])
        #pragma acc exit data delete(s->nl[0:s->n_beads_memory*s->nl_len_max])
        #pragma acc exit data delete(s->nl_len[0:s->n_beads_memory])
        #pragma acc exit data delete(s->id_cell[0:s->n_beads_memory])
        #pragma acc exit data delete(s->type[0:s->n_beads_memory])

        #pragma acc exit data delete(s->id_orig[0:s->n_beads_memory])
        #pragma acc exit data delete(s->id_sort[0:s->n_beads_memory])
        #pragma acc exit data delete(s->id_unsort[0:s->n_beads_memory])

        free(s->r);
        free(s->im);
        free(s->p);
        free(s->f);
        free(s->rho);
        free(s->r_check);
        free(s->im_check);
        free(s->nl);
        free(s->bl);
        free(s->nl_len);
        free(s->bl_len);
        free(s->id_cell);
        free(s->type);

        free(s->id_orig);
        free(s->id_sort);
        free(s->id_unsort);

        const int n_beads_memory_old = s->n_beads_memory;
        s->n_beads_memory = (int)nearbyint(n_beads_new * 1.3);

        s->r = malloc(s->n_beads_memory * sizeof *s->r);
        s->im = malloc(s->n_beads_memory * sizeof *s->im);
        s->r_check = malloc(s->n_beads_memory * sizeof *s->r_check);
        s->im_check = malloc(s->n_beads_memory * sizeof *s->im_check);
        s->p = malloc(s->n_beads_memory * sizeof *s->p);
        s->f = malloc(s->n_beads_memory * sizeof *s->f);
        s->rho = malloc(s->n_beads_memory * sizeof *s->rho);
        s->bl = malloc(s->n_beads_memory * s->bl_len_max * sizeof *s->bl);
        s->nl = malloc(s->n_beads_memory * s->nl_len_max * sizeof *s->nl);
        s->bl_len = malloc(s->n_beads_memory * sizeof *s->bl_len);
        s->nl_len = malloc(s->n_beads_memory * sizeof *s->nl_len);
        s->id_cell = malloc(s->n_beads_memory * sizeof *s->id_cell);
        s->type = malloc(s->n_beads_memory * sizeof *s->type);

        s->id_orig = malloc(s->n_beads_memory * sizeof *s->id_orig);
        s->id_sort = malloc(s->n_beads_memory * sizeof *s->id_sort);
        s->id_unsort = malloc(s->n_beads_memory * sizeof *s->id_unsort);

        #pragma acc enter data create(s->r[0:s->n_beads_memory])
        #pragma acc enter data create(s->r_check[0:s->n_beads_memory])
        #pragma acc enter data create(s->p[0:s->n_beads_memory])
        #pragma acc enter data create(s->f[0:s->n_beads_memory])
        #pragma acc enter data create(s->rho[0:s->n_beads_memory])
        #pragma acc enter data create(s->im[0:s->n_beads_memory])
        #pragma acc enter data create(s->im_check[0:s->n_beads_memory])
        #pragma acc enter data create(s->bl[0:s->n_beads_memory*s->bl_len_max])
        #pragma acc enter data create(s->bl_len[0:s->n_beads_memory])
        #pragma acc enter data create(s->nl[0:s->n_beads_memory*s->nl_len_max])
        #pragma acc enter data create(s->nl_len[0:s->n_beads_memory])
        #pragma acc enter data create(s->id_cell[0:s->n_beads_memory])
        #pragma acc enter data create(s->type[0:s->n_beads_memory])

        #pragma acc enter data create(s->id_orig[0:s->n_beads_memory])
        #pragma acc enter data create(s->id_sort[0:s->n_beads_memory])
        #pragma acc enter data create(s->id_unsort[0:s->n_beads_memory])

        #pragma acc parallel loop present(s[0:1])
        for(int i = 0; i < n_beads_memory_old; i++)
        {
            s->id_orig[i] = s->id_orig_copy[i];

            #pragma acc loop seq
            for(int d = 0; d < 3; d++)
            {
                s->r[i][d] = s->r_copy[i][d];
                s->p[i][d] = s->p_copy[i][d];
                s->im[i][d] = s->im_copy[i][d];
            }

            s->bl_len[i] = s->bl_len_copy[i];
            s->type[i] = s->type_copy[i];

            #pragma acc loop seq
            for(int b0 = 0; b0 < s->bl_len[i]; b0++)
            {
                s->bl[i * s->bl_len_max + b0] = s->bl_copy[i * s->bl_len_max + b0];
            }
        }

        #pragma acc exit data delete(s->r_copy[0:n_beads_memory_old])
        #pragma acc exit data delete(s->p_copy[0:n_beads_memory_old])
        #pragma acc exit data delete(s->f_copy[0:n_beads_memory_old])
        #pragma acc exit data delete(s->im_copy[0:n_beads_memory_old])
        #pragma acc exit data delete(s->bl_copy[0:n_beads_memory_old*s->bl_len_max])
        #pragma acc exit data delete(s->bl_len_copy[0:n_beads_memory_old])
        #pragma acc exit data delete(s->id_cell_copy[0:n_beads_memory_old])
        #pragma acc exit data delete(s->id_orig_copy[0:n_beads_memory_old])
        #pragma acc exit data delete(s->type_copy[0:n_beads_memory_old])

        free(s->r_copy);
        free(s->im_copy);
        free(s->p_copy);
        free(s->f_copy);
        free(s->bl_copy);
        free(s->bl_len_copy);
        free(s->id_cell_copy);
        free(s->id_orig_copy);
        free(s->type_copy);

        s->r_copy = malloc(s->n_beads_memory * sizeof *s->r_copy);
        s->im_copy = malloc(s->n_beads_memory * sizeof *s->im_copy);
        s->p_copy = malloc(s->n_beads_memory * sizeof *s->p_copy);
        s->f_copy = malloc(s->n_beads_memory * sizeof *s->f_copy);
        s->bl_copy = malloc(s->n_beads_memory * s->bl_len_max * sizeof *s->bl_copy);
        s->bl_len_copy = malloc(s->n_beads_memory * sizeof *s->bl_len_copy);
        s->id_cell_copy = malloc(s->n_beads_memory * sizeof *s->id_cell_copy);
        s->id_orig_copy = malloc(s->n_beads_memory * sizeof *s->id_orig_copy);
        s->type_copy = malloc(s->n_beads_memory * sizeof *s->type_copy);

        #pragma acc enter data create(s->r_copy[0:s->n_beads_memory])
        #pragma acc enter data create(s->p_copy[0:s->n_beads_memory])
        #pragma acc enter data create(s->f_copy[0:s->n_beads_memory])
        #pragma acc enter data create(s->im_copy[0:s->n_beads_memory])
        #pragma acc enter data create(s->bl_copy[0:s->n_beads_memory*s->bl_len_max])
        #pragma acc enter data create(s->bl_len_copy[0:s->n_beads_memory])
        #pragma acc enter data create(s->id_cell_copy[0:s->n_beads_memory])
        #pragma acc enter data create(s->id_orig_copy[0:s->n_beads_memory])
        #pragma acc enter data create(s->type_copy[0:s->n_beads_memory])

        int imax = -1;

        #pragma acc parallel loop present(s[0:1]) reduction(max:imax)
        for(int i = 0; i < s->n_beads; i++)
        {
            if(s->id_orig[i] > imax)
            {
                imax = s->id_orig[i];
            }
        }

        imax++;

        if(imax < s->n_beads_memory)
        {
            imax = s->n_beads_memory;
        }

        if(imax > s->id_orig_rev_size)
        {
            #pragma acc exit data delete(s->id_orig_rev[0:s->id_orig_rev_size])
            free(s->id_orig_rev);
            s->id_orig_rev_size = imax + 1;
            #pragma acc update device(s->id_orig_rev_size)
            s->id_orig_rev = malloc(s->id_orig_rev_size * sizeof *s->id_orig_rev);
            #pragma acc enter data create(s->id_orig_rev[0:s->id_orig_rev_size])
        }

        // s->defragmentation_needed = 1;
        return 1;
    }
    else
    {
        return 0;
    }
}

void delete_bead(MySystem * s, const int i)
{
    const int j = s->n_beads - 1;

    #pragma acc update host(s->type[i])
    #pragma acc update host(s->type[j])

    if(s->type[i] != 1 || s->type[j] != 1)
    {
        printf("ERROR: Only particles of type 1 can be deleted!\n");
        exit(-1);
    }

    #pragma acc update host(s->r[j])
    #pragma acc update host(s->p[j])
    #pragma acc update host(s->f[j])
    #pragma acc update host(s->im[j])
    #pragma acc update host(s->id_orig[j])

    for(int d = 0; d < 3; d++)
    {
        s->r[i][d] = s->r[j][d];
        s->p[i][d] = s->p[j][d];
        s->f[i][d] = s->f[j][d];
        s->im[i][d] = s->im[j][d];
    }

    s->id_orig[i] = s->id_orig[j];

    #pragma acc update device(s->r[i])
    #pragma acc update device(s->p[i])
    #pragma acc update device(s->f[i])
    #pragma acc update device(s->im[i])
    #pragma acc update device(s->id_orig[i])
}

void assign_new_tags(MySystem * s, int n_beads_old, int n_beads)
{
    #pragma acc update host(s->id_orig[0:s->n_beads])

    for(int i = 0; i < s->n_beads_memory; i++)
    {
        s->id_orig_rev[i] = 1; // use id_orig_rev as temporary array to memorize wether a tag is free to give
    }

    for(int i = 0; i < n_beads_old; i++)
    {
        s->id_orig_rev[s->id_orig[i]] = 0; // the tags of all old beads are of course already taken
    }

    int last_free_tag = -1;

    for(int i = n_beads_old; i < s->n_beads; i++)
    {
        int j;

        for(j = last_free_tag + 1; j < s->n_beads_memory && s->id_orig_rev[j] == 0; j++);

        s->id_orig[i] = j;
        s->id_orig_rev[j] = 0;
        last_free_tag = j;
    }

    #pragma acc update device(s->id_orig[0:s->n_beads])
}
