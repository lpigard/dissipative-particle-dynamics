int set_vapor_density(MySystem * s);
void reinsert_bulk(MySystem * s);
void calc_h_crit(MySystem * s);
void healing(MySystem * s);
int reallocate_bead_data(MySystem * s, const int n_beads_new);
void delete_bead(MySystem * s, const int i);
void assign_new_tags(MySystem * s, int n_beads_old, int n_beads);
