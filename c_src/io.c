// handling of I/O, including reading command-line parameters, setting default values of certain values, reading input configuration HDF5 files and writing output configuration HDF5, ana as well as ana_field files. Also allocating and deallocating memory on main and GPU memory.

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#include <assert.h>
#ifdef _OPENACC
#include <openacc.h>
#endif
#include "system.h"
#include "io.h"
#include "integrate.h"
#include "ana.h"
#include "ana_fields.h"
#include "mesh.h"

volatile int NoKeyboardInterrupt = 1;

void intHandler(int dummy)
{
    printf("Catched Signal to Stop.\n");
    NoKeyboardInterrupt = 0;
}

// allocating main memory and copy to GPU
int initialize_MySystem(MySystem * s)
{
    signal(SIGTSTP, intHandler);

    assert(access(s->conf_file_name, F_OK) == 0);

    read_conf_scalars(s);
    s->time_0 = s->time;
    s->dt = s->dt_dbl;

    // ##### Fix the following parameters for now #####
    s->r_buffer2 = 0.3 * 0.3;
    s->info_dt = 10;
    s->ana_dt_int = 100;
    s->sample_fields_dt_int = 100;
    s->ana_fields_dt_int = 100 * s->sample_fields_dt_int;
    s->reinsert_bulk_dt_int = s->ana_fields_dt_int; // synchronize field calculation with reinsertion of bulk material for now; this ensures correct calculation of h_crit
    s->walltime = 86000;
    s->dz_fields = 0.5;
    s->n_bins_fields = (int)(s->L[2] / s->dz_fields);
    s->peters_dt_int = 100;
    s->integration_scheme = 0;
    s->h_crit_min = s->wall_thickness / 2. + 10 * s->sigma_wall * pow(2. / 5., 1. / 6.);
    if(s->h_crit_min < 20)
    {
        s->h_crit_min = 20;
    }

    s->vapor_zone_dt_int = 5;
    s->z0_vapor_zone = 10.;
    s->dz_min_vapor_zone = 30.;
    s->dt_healing = 100.;
    s->t_healing = s->time;
    s->defragment_memory_dt_int = 1000000;
    s->mc_dt_int = 100;

    // ################################################

    if(s->integration_scheme < 0 || s->integration_scheme > 2)
    {
        printf("ERROR: Invalid integration_scheme (%d). Use 0 = DPD, 1 = Peters 1, 2 = Peters 2!\n", s->integration_scheme);
        return -1;
    }

    s->sim_time_int = (int)nearbyint(s->sim_time_dbl / s->dt_dbl);
    s->conf_save_dt_int = (int)nearbyint(s->conf_save_dt_real / s->dt);

    assert(s->reinsert_bulk_dt_int % s->vapor_zone_dt_int == 0);

    s->n_beads_memory = s->n_beads;
    int n_beads = s->n_beads;
    int n_types = s->n_types;
    int n_nanos = s->n_nanos;

    s->r = malloc(n_beads * sizeof *s->r);
    s->im = malloc(n_beads * sizeof *s->im);
    s->r_check = malloc(n_beads * sizeof *s->r_check);
    s->im_check = malloc(n_beads * sizeof *s->im_check);
    s->p = malloc(n_beads * sizeof *s->p);
    s->f = malloc(n_beads * sizeof *s->f);
    s->rho = malloc(n_beads * sizeof *s->rho);
    s->bl = malloc(n_beads * s->bl_len_max * sizeof *s->bl);
    s->nl = malloc(n_beads * s->nl_len_max * sizeof *s->nl);
    s->bl_len = malloc(n_beads * sizeof *s->bl_len);
    s->nl_len = malloc(n_beads * sizeof *s->nl_len);
    s->nano_list = malloc(n_nanos * sizeof *s->nano_list);
    s->id_cell = malloc(n_beads * sizeof *s->id_cell);
    s->type = malloc(n_beads * sizeof *s->type);
    s->gamma = malloc(n_types * n_types * sizeof *s->gamma);
    s->rc = malloc(n_types * n_types * sizeof *s->rc);
    s->a = malloc(n_types * n_types * sizeof *s->a);
    s->kb = malloc(n_types * n_types * sizeof *s->kb);
    s->r0 = malloc(n_types * n_types * sizeof *s->r0);
    s->epsilon_lj = malloc(n_types * n_types * sizeof *s->epsilon_lj);
    s->sigma_lj = malloc(n_types * n_types * sizeof *s->sigma_lj);
    s->epsilon_wall_bot = malloc(n_types * sizeof *s->epsilon_wall_bot);
    s->epsilon_wall_top = malloc(n_types * sizeof *s->epsilon_wall_top);
    s->density_field = malloc(s->n_bins_fields * s->n_types * sizeof *s->density_field);
    s->temperature_field = malloc(s->n_bins_fields * sizeof *s->temperature_field);
    s->pressure_field = malloc(s->n_bins_fields * sizeof *s->pressure_field);
    s->velocity_field = malloc(s->n_bins_fields * s->n_types * sizeof *s->velocity_field);

    s->id_orig = malloc(n_beads * sizeof *s->id_orig);
    s->id_sort = malloc(n_beads * sizeof *s->id_sort);
    s->id_unsort = malloc(n_beads * sizeof *s->id_unsort);

    s->r_copy = malloc(n_beads * sizeof *s->r_copy);
    s->im_copy = malloc(n_beads * sizeof *s->im_copy);
    s->p_copy = malloc(n_beads * sizeof *s->p_copy);
    s->f_copy = malloc(n_beads * sizeof *s->f_copy);
    s->bl_copy = malloc(n_beads * s->bl_len_max * sizeof *s->bl_copy);
    s->bl_len_copy = malloc(n_beads * sizeof *s->bl_len_copy);
    s->id_cell_copy = malloc(n_beads * sizeof *s->id_cell_copy);
    s->id_orig_copy = malloc(n_beads * sizeof *s->id_orig_copy);
    s->type_copy = malloc(n_beads * sizeof *s->type_copy);

    printf("INFO: Take initial condition from configuration file %s.\n", s->conf_file_name);
    read_conf_arrays(s);
    init_mesh(s);

    int imax = -1;

    for(int i = 0; i < s->n_beads; i++)
    {
        if(s->id_orig[i] > imax)
        {
            imax = s->id_orig[i];
        }
    }

    s->id_orig_rev_size = imax + 1;
    s->id_orig_rev = malloc(s->id_orig_rev_size * sizeof *s->id_orig_rev);

    s->n_sample_fields = 0;
    s->delta_h_reinsertion = 0.;
    s->n_beads_removed = 0;

    const int nz = s->n_bins_fields;
    const int nt = s->n_types;

    for(int z = 0; z < nz; z++)
    {
        for(int d1 = 0; d1 < 3; d1++)
        {
            for(int d2 = 0; d2 < 3; d2++)
            {
                s->temperature_field[z][d1][d2] = 0;
                s->pressure_field[z][d1][d2] = 0;
            }
        }

        for(int t = 0; t < nt; t++)
        {
            const int ind = z * nt + t;
            s->density_field[ind] = 0.;

            for(int d = 0; d < 3; d++)
            {
                const int ind = z * nt + t;
                s->velocity_field[ind][d] = 0;
            }
        }
    }

    #ifdef _OPENACC
    const int ngpus = acc_get_num_devices(acc_device_nvidia);
    if(ngpus)
    {
        printf("INFO: Running on 1 CPU accelerated by the following GPU:\n");
        for(int i = 0; i < ngpus; i++)
        {
            printf("\t%s with %.3g GB of memory and driver version %s\n", acc_get_property_string(0, acc_device_nvidia, acc_property_name), (double)acc_get_property(0, acc_device_nvidia, acc_property_memory) / 1e9, acc_get_property_string(0, acc_device_nvidia, acc_property_driver));
        }
    }
    else
    {
        printf("WARNING: No Nvidia GPU found for accceleration! Running on 1 CPU with one thread.\n");
    }
    #endif

    if(access(s->ana_file_name, F_OK))
    {
        char str[1048576];

        printf("Analyzing the following quantities and save them to %s:\n", s->ana_file_name);
        sprintf(str, "[0]time [1]e_kin [2]e_bl e_bend[3] [4]e_nl [5]e_tot [6]e_delta/e_tot [7]px_tot [8]py_tot [9]pz_tot [10]T_kin [11]cl_mean [12]cl_std [13]nl_mean [14]nl_std [15]dt_int_nl_rebuild [16]bead_msd_x [17]bead_msd_y [18]bead_msd_z [19]poly_msd_x [20]poly_msd_y [21]poly_msd_z [22]re2_x [23]re2_y [24]re2_z [25]rg2_x [26]rg2_y [27]rg2_z [28]lc [29]pressure [30]h [31]surface_tension [32]n_beads_removed [33]delta_h_reinsertion [34]h_crit [35]Lz [36]angle_mean [37]angle_std [38]acceptance_ratio [39]order_parameter [40]h0\n");

        printf("%s", str);

        FILE * f = fopen(s->ana_file_name, "w");
        fprintf(f, "%s", str);
        fclose(f);
    }

    s->ana_file_pointer = fopen(s->ana_file_name, "a");

    init_rest(s);

    if(access(s->bulk_file_name, F_OK))
    {
        printf("INFO: %s does not exist! No particles will be reinserted in evaporation process.\n", s->bulk_file_name);
        s->reinsert_bulk = 0;
        s->n_beads_bulk = -1;
        s->Lz_bulk = -1.;
        s->h_crit = -1.;
        s->phi0 = -1.;
        s->delta_phi_crit = -1.;
    }
    else
    {
        s->reinsert_bulk = 1;
        s->h_crit = s->h_crit_min;

        hid_t file_id = H5Fopen(s->bulk_file_name, H5F_ACC_RDONLY, H5P_DEFAULT);

        real L_bulk[3];

        read_dataset(file_id, &s->n_beads_bulk, "n_beads", H5T_NATIVE_INT);
        read_dataset(file_id, L_bulk, "L", H5T_NATIVE_REAL);
         s->Lz_bulk = L_bulk[2];
        int * type_bulk = malloc(s->n_beads_bulk * sizeof(int));
        read_dataset(file_id, type_bulk, "type", H5T_NATIVE_INT);
        int sum = 0;
        for(int i = 0; i < s->n_beads_bulk; i++)
        {
            if(type_bulk[i] < s->n_types_non_evaporating)
            {
                sum++;
            }
        }
        free(type_bulk);
        s->phi0 = (real) sum / s->n_beads_bulk;
        H5Fclose(file_id);

        const real dz = s->dz_fields;

        if(s->phi0 > 1e-8)
        {
            s->delta_phi_crit = 2. / sqrt(s->phi0 * dz * s->L[0] * s->L[1] * (real)s->ana_fields_dt_int / s->sample_fields_dt_int);

            if(s->delta_phi_crit > 1. / 2.)
            {
                printf("ERROR: delta_phi_crit=%.7g is too big. Increase number of particles per bin for density_field! This is necessary to ensure correct calculation of h_crit with thermal fluctuations\n", s->delta_phi_crit);
                return -1;
            }
        }
        else
        {
            s->delta_phi_crit = 2.0;
        }

        printf("INFO: Reinsert %d particles (phi_0 = %.7g) with h_crit_min = %.7g, delta_phi_crit = %.7g and reinsert_bulk_dt_int = %d from file %s.\n", s->n_beads_bulk, s->phi0, s->h_crit_min, s->delta_phi_crit, s->reinsert_bulk_dt_int, s->bulk_file_name);
    }

    #pragma acc enter data copyin(s[0:1])
    #pragma acc enter data copyin(s->r[0:s->n_beads])
    #pragma acc enter data create(s->r_check[0:s->n_beads])
    #pragma acc enter data copyin(s->p[0:s->n_beads])
    #pragma acc enter data copyin(s->f[0:s->n_beads])
    #pragma acc enter data create(s->rho[0:s->n_beads])
    #pragma acc enter data copyin(s->im[0:s->n_beads])
    #pragma acc enter data create(s->im_check[0:s->n_beads])
    #pragma acc enter data copyin(s->bl[0:s->n_beads*s->bl_len_max])
    #pragma acc enter data copyin(s->bl_len[0:s->n_beads])
    #pragma acc enter data create(s->nl[0:s->n_beads*s->nl_len_max])
    #pragma acc enter data create(s->nl_len[0:s->n_beads])
    #pragma acc enter data copyin(s->cell_neighbor[0:s->n_cells])
    #pragma acc enter data copyin(s->nano_list[0:s->n_nanos])
    #pragma acc enter data create(s->id_cell[0:s->n_beads])
    #pragma acc enter data copyin(s->type[0:s->n_beads])
    #pragma acc enter data copyin(s->gamma[0:s->n_types*s->n_types])
    #pragma acc enter data copyin(s->rc[0:s->n_types*s->n_types])
    #pragma acc enter data copyin(s->kb[0:s->n_types*s->n_types])
    #pragma acc enter data copyin(s->r0[0:s->n_types*s->n_types])
    #pragma acc enter data copyin(s->a[0:s->n_types*s->n_types])
    #pragma acc enter data copyin(s->epsilon_lj[0:s->n_types*s->n_types])
    #pragma acc enter data copyin(s->sigma_lj[0:s->n_types*s->n_types])
    #pragma acc enter data copyin(s->epsilon_wall_bot[0:s->n_types])
    #pragma acc enter data copyin(s->epsilon_wall_top[0:s->n_types])
    #pragma acc enter data create(s->density_z[0:s->n[2]])
    #pragma acc enter data copyin(s->density_field[0:s->n_bins_fields*s->n_types])
    #pragma acc enter data copyin(s->temperature_field[0:s->n_bins_fields])
    #pragma acc enter data copyin(s->pressure_field[0:s->n_bins_fields])
    #pragma acc enter data copyin(s->velocity_field[0:s->n_bins_fields*s->n_types])

    #pragma acc enter data create(s->cell_begin[0:s->n_cells])
    #pragma acc enter data create(s->cell_end[0:s->n_cells])
    #pragma acc enter data copyin(s->id_orig[0:s->n_beads])
    #pragma acc enter data create(s->id_orig_rev[0:s->id_orig_rev_size])
    #pragma acc enter data create(s->id_sort[0:s->n_beads])
    #pragma acc enter data create(s->id_unsort[0:s->n_beads])

    #pragma acc enter data create(s->r_copy[0:s->n_beads])
    #pragma acc enter data create(s->p_copy[0:s->n_beads])
    #pragma acc enter data create(s->f_copy[0:s->n_beads])
    #pragma acc enter data create(s->im_copy[0:s->n_beads])
    #pragma acc enter data create(s->bl_copy[0:s->n_beads*s->bl_len_max])
    #pragma acc enter data create(s->bl_len_copy[0:s->n_beads])
    #pragma acc enter data create(s->id_cell_copy[0:s->n_beads])
    #pragma acc enter data create(s->id_orig_copy[0:s->n_beads])
    #pragma acc enter data create(s->type_copy[0:s->n_beads])

    calc_nl(s);
    calc_forces(s);

    printf("########################################################################################################################\n");
    printf("INFO: Start simulation with following parameters:\n");

    printf("n_beads = %d \n", s->n_beads);
    printf("n_types = %d \n", s->n_types);
    printf("n_nanos = %d \n", s->n_nanos);
    printf("bl_len_max = %d \n", s->bl_len_max);
    printf("Lx = %.7g \n", s->L[0]);
    printf("Ly = %.7g \n", s->L[1]);
    printf("Lz = %.7g \n", s->L[2]);
    printf("time = %.12g \n", s->time);
    printf("kT = %.7g \n", s->kT);
    printf("rd = %.7g \n", s->rd);
    printf("b = %.7g \n", s->b);
    printf("k_bend = %.7g \n", s->k_bend);
    printf("sigma_wall = %.7g \n", s->sigma_wall);
    printf("wall_thickness = %.7g \n", s->wall_thickness);
    printf("r_buffer = %.7g \n", sqrt(s->r_buffer2));
    printf("nx = %d \n", s->n[0]);
    printf("ny = %d \n", s->n[1]);
    printf("nz = %d \n", s->n[2]);
    printf("dt = %.7g \n", s->dt);
    printf("conf_save_dt_real = %.7g \n", s->conf_save_dt_real);
    printf("ana_dt_int = %d \n", s->ana_dt_int);
    printf("ana_fields_dt_int = %d \n", s->ana_fields_dt_int);
    printf("info_dt = %.7g \n", s->info_dt);
    printf("dz_min_vapor_zone = %.7g \n", s->dz_min_vapor_zone);
    printf("z0_vapor_zone = %.7g \n", s->z0_vapor_zone);
    printf("rho_vapor_zone = %.7g \n", s->rho_vapor_zone);
    printf("vapor_zone_dt_int = %d \n", s->vapor_zone_dt_int);
    printf("n_bins_fields = %d \n", s->n_bins_fields);
    printf("sample_fields_dt_int = %d \n", s->sample_fields_dt_int);
    printf("peters_dt_int = %d \n", s->peters_dt_int);
    printf("integration_scheme = %d \n", s->integration_scheme);
    printf("walltime = %d \n", s->walltime);
    printf("h_crit_min = %.7g \n", s->h_crit_min);
    printf("nl_len_max = %d \n", s->nl_len_max);
    printf("r_nl = %.7g \n", sqrt(s->r_nl2));

    printf("rc = \n");
    for(int i = 0; i < n_types; i++)
    {
        for(int j = 0; j < n_types; j++)
        {
            printf("%.7g\t", s->rc[i * n_types + j]);
        }
        printf("\n");
    }

    printf("gamma = \n");
    for(int i = 0; i < n_types; i++)
    {
        for(int j = 0; j < n_types; j++)
        {
            printf("%.7g\t", s->gamma[i * n_types + j]);
        }
        printf("\n");
    }

    printf("a = \n");
    for(int i = 0; i < n_types; i++)
    {
        for(int j = 0; j < n_types; j++)
        {
            printf("%.7g\t", s->a[i * n_types + j]);
        }
        printf("\n");
    }

    printf("epsilon_lj = \n");
    for(int i = 0; i < n_types; i++)
    {
        for(int j = 0; j < n_types; j++)
        {
            printf("%.7g\t", s->epsilon_lj[i * n_types + j]);
        }
        printf("\n");
    }

    printf("sigma_lj = \n");
    for(int i = 0; i < n_types; i++)
    {
        for(int j = 0; j < n_types; j++)
        {
            printf("%.7g\t", s->sigma_lj[i * n_types + j]);
        }
        printf("\n");
    }

    printf("kb = \n");
    for(int i = 0; i < n_types; i++)
    {
        for(int j = 0; j < n_types; j++)
        {
            printf("%.7g\t", s->kb[i * n_types + j]);
        }
        printf("\n");
    }

    printf("r0 = \n");
    for(int i = 0; i < n_types; i++)
    {
        for(int j = 0; j < n_types; j++)
        {
            printf("%.7g\t", s->r0[i * n_types + j]);
        }
        printf("\n");
    }

    printf("epsilon_wall_bot = \n");
    for(int i = 0; i < n_types; i++)
    {
        printf("%.7g\t", s->epsilon_wall_bot[i]);
    }
    printf("\n");

    printf("epsilon_wall_top = \n");
    for(int i = 0; i < n_types; i++)
    {
        printf("%.7g\t", s->epsilon_wall_top[i]);
    }
    printf("\n");

    printf("########################################################################################################################\n");
    return 0;
}

// deallocating main and gpu memory
void finalize_MySystem(MySystem * s)
{
    printf("Clean up memory.\n");
    fclose(s->ana_file_pointer);

    #pragma acc exit data delete(s->r[0:s->n_beads_memory])
    #pragma acc exit data delete(s->r_check[0:s->n_beads_memory])
    #pragma acc exit data delete(s->p[0:s->n_beads_memory])
    #pragma acc exit data delete(s->f[0:s->n_beads_memory])
    #pragma acc exit data delete(s->rho[0:s->n_beads_memory])
    #pragma acc exit data delete(s->im[0:s->n_beads_memory])
    #pragma acc exit data delete(s->im_check[0:s->n_beads_memory])
    #pragma acc exit data delete(s->bl[0:s->n_beads_memory*s->bl_len_max])
    #pragma acc exit data delete(s->bl_len[0:s->n_beads_memory])
    #pragma acc exit data delete(s->nl[0:s->n_beads_memory*s->nl_len_max])
    #pragma acc exit data delete(s->nl_len[0:s->n_beads_memory])
    #pragma acc exit data delete(s->nano_list[0:s->n_nanos])
    #pragma acc exit data delete(s->id_cell[0:s->n_beads_memory])
    #pragma acc exit data delete(s->type[0:s->n_beads_memory])
    #pragma acc exit data delete(s->gamma[0:s->n_types*s->n_types])
    #pragma acc exit data delete(s->rc[0:s->n_types*s->n_types])
    #pragma acc exit data delete(s->a[0:s->n_types*s->n_types])
    #pragma acc exit data delete(s->epsilon_lj[0:s->n_types*s->n_types])
    #pragma acc exit data delete(s->sigma_lj[0:s->n_types*s->n_types])
    #pragma acc exit data delete(s->kb[0:s->n_types*s->n_types])
    #pragma acc exit data delete(s->r0[0:s->n_types*s->n_types])
    #pragma acc exit data delete(s->epsilon_wall_bot[0:s->n_types])
    #pragma acc exit data delete(s->epsilon_wall_top[0:s->n_types])

    #pragma acc exit data delete(s->density_field[0:s->n_bins_fields*s->n_types])
    #pragma acc exit data delete(s->temperature_field[0:s->n_bins_fields])
    #pragma acc exit data delete(s->pressure_field[0:s->n_bins_fields])
    #pragma acc exit data delete(s->velocity_field[0:s->n_bins_fields*s->n_types])

    #pragma acc exit data delete(s->id_orig[0:s->n_beads_memory])
    #pragma acc exit data delete(s->id_orig_rev[0:s->id_orig_rev_size])
    #pragma acc exit data delete(s->id_sort[0:s->n_beads_memory])
    #pragma acc exit data delete(s->id_unsort[0:s->n_beads_memory])

    #pragma acc exit data delete(s->r_copy[0:s->n_beads_memory])
    #pragma acc exit data delete(s->p_copy[0:s->n_beads_memory])
    #pragma acc exit data delete(s->f_copy[0:s->n_beads_memory])
    #pragma acc exit data delete(s->im_copy[0:s->n_beads_memory])
    #pragma acc exit data delete(s->bl_copy[0:s->n_beads_memory*s->bl_len_max])
    #pragma acc exit data delete(s->bl_len_copy[0:s->n_beads_memory])
    #pragma acc exit data delete(s->id_cell_copy[0:s->n_beads_memory])
    #pragma acc exit data delete(s->id_orig_copy[0:s->n_beads_memory])
    #pragma acc exit data delete(s->type_copy[0:s->n_beads_memory])

    finalize_mesh(s);

    #pragma acc exit data delete(s[0:1])

    free(s->r);
    free(s->im);
    free(s->p);
    free(s->f);
    free(s->rho);
    free(s->r_check);
    free(s->im_check);
    free(s->nl);
    free(s->bl);
    free(s->nl_len);
    free(s->bl_len);
    free(s->nano_list);
    free(s->id_cell);
    free(s->type);
    free(s->gamma);
    free(s->rc);
    free(s->kb);
    free(s->r0);
    free(s->a);
    free(s->epsilon_lj);
    free(s->sigma_lj);
    free(s->epsilon_wall_bot);
    free(s->epsilon_wall_top);
    free(s->density_field);
    free(s->temperature_field);
    free(s->pressure_field);
    free(s->velocity_field);

    free(s->id_orig);
    free(s->id_orig_rev);
    free(s->id_sort);
    free(s->id_unsort);

    free(s->r_copy);
    free(s->im_copy);
    free(s->p_copy);
    free(s->f_copy);
    free(s->bl_copy);
    free(s->bl_len_copy);
    free(s->id_cell_copy);
    free(s->id_orig_copy);
    free(s->type_copy);
}

// initializing rest of parameters (to arbitrary values); only used to shut up debugger
void init_rest(MySystem * s)
{
    s->e_bl = 0.;
    s->e_bend = 0.;
    s->e_nl = 0.;
    s->e_kin = 0.;
    s->e_tot = 0.;
    s->e_delta = 0.;
    s->p_tot[0] = 0.;
    s->p_tot[1] = 0.;
    s->p_tot[2] = 0.;
    s->T_kin = 0.;
    s->cl_mean = 0.;
    s->cl_std = 0.;
    s->nl_mean = 0.;
    s->nl_std = 0.;
    s->lc = 0.;
    s->pressure = 0.;
    s->surface_tension = 0.;
    s->bead_msd[0] = 0.;
    s->bead_msd[1] = 0.;
    s->bead_msd[2] = 0.;
    s->poly_msd[0] = 0.;
    s->poly_msd[1] = 0.;
    s->poly_msd[2] = 0.;
    s->re2[0] = 0.;
    s->re2[1] = 0.;
    s->re2[2] = 0.;
    s->rg2[0] = 0.;
    s->rg2[1] = 0.;
    s->rg2[2] = 0.;
    s->h = 0.;
}

// reading arrays from configuration hdf5 file; NOTE: arrays have to be allocated before (this also means that read_conf_scalars must be called beforehand)
void read_conf_arrays(MySystem * s)
{
    hid_t file_id = H5Fopen(s->conf_file_name, H5F_ACC_RDONLY, H5P_DEFAULT);

    read_dataset(file_id, s->r, "r", H5T_NATIVE_REAL);
    read_dataset(file_id, s->im, "im", H5T_NATIVE_INT);
    read_dataset(file_id, s->p, "p", H5T_NATIVE_REAL);
    read_dataset(file_id, s->gamma, "gamma", H5T_NATIVE_REAL);
    read_dataset(file_id, s->rc, "rc", H5T_NATIVE_REAL);
    read_dataset(file_id, s->a, "a", H5T_NATIVE_REAL);
    read_dataset(file_id, s->epsilon_lj, "epsilon_lj", H5T_NATIVE_REAL);
    read_dataset(file_id, s->sigma_lj, "sigma_lj", H5T_NATIVE_REAL);
    read_dataset(file_id, s->kb, "kb", H5T_NATIVE_REAL);
    read_dataset(file_id, s->r0, "r0", H5T_NATIVE_REAL);
    read_dataset(file_id, s->bl, "bl", H5T_NATIVE_INT);
    read_dataset(file_id, s->bl_len, "bl_len", H5T_NATIVE_INT);
    read_dataset(file_id, s->nano_list, "nano_list", H5T_NATIVE_INT);
    read_dataset(file_id, s->type, "type", H5T_NATIVE_INT);
    read_dataset(file_id, s->id_orig, "id_orig", H5T_NATIVE_INT);
    read_dataset(file_id, s->epsilon_wall_bot, "epsilon_wall_bot", H5T_NATIVE_REAL);
    read_dataset(file_id, s->epsilon_wall_top, "epsilon_wall_top", H5T_NATIVE_REAL);

    H5Fclose(file_id);
}

// reading scalar parameters from configuration hdf5 file;
void read_conf_scalars(MySystem * s)
{
    hid_t file_id = H5Fopen(s->conf_file_name, H5F_ACC_RDONLY, H5P_DEFAULT);

    read_dataset(file_id, &s->n_types, "n_types", H5T_NATIVE_INT);
    read_dataset(file_id, &s->n_types_non_evaporating, "n_types_non_evaporating", H5T_NATIVE_INT);
    read_dataset(file_id, &s->n_beads, "n_beads", H5T_NATIVE_INT);
    read_dataset(file_id, &s->n_nanos, "n_nanos", H5T_NATIVE_INT);
    read_dataset(file_id, &s->bl_len_max, "bl_len_max", H5T_NATIVE_INT);
    read_dataset(file_id, s->L, "L", H5T_NATIVE_REAL);
    read_dataset(file_id, &s->time, "time", H5T_NATIVE_DOUBLE);
    read_dataset(file_id, &s->kT, "kT", H5T_NATIVE_REAL);
    read_dataset(file_id, &s->rd, "rd", H5T_NATIVE_REAL);
    read_dataset(file_id, &s->b, "b", H5T_NATIVE_REAL);
    read_dataset(file_id, &s->k_bend, "k_bend", H5T_NATIVE_REAL);
    read_dataset(file_id, &s->sigma_wall, "sigma_wall", H5T_NATIVE_REAL);
    read_dataset(file_id, &s->wall_thickness, "wall_thickness", H5T_NATIVE_REAL);

    H5Fclose(file_id);
}

// save configurational hdf5 file
void save_conf(MySystem * s, int is_restarting)
{
    char fn[1024];
    char gn[1024];
    hid_t file_id;
    hid_t group_id;
    hid_t data_id;

    if(is_restarting) // single restart file; allows the user to continue a previous simulation
    {
        sprintf(fn, "%s_conf_restart.h5", s->prefix);
        file_id = H5Fcreate(fn, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
        data_id = file_id;
    }
    else // or write the configuration to a hdf5 file that collects many configurations over simulation time (useful for post-analysis)
    {
        sprintf(fn, "%s_conf.h5", s->prefix);

        if(access(fn, F_OK) == 0)
        {
            file_id = H5Fopen(fn, H5F_ACC_RDWR, H5P_DEFAULT);
        }
        else
        {
            file_id = H5Fcreate(fn, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
        }

        sprintf(gn, "/t%.12g", s->time);
        group_id = H5Gcreate(file_id, gn, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
        data_id = group_id;
    }

    #pragma acc update host(s->r[0:s->n_beads])
    #pragma acc update host(s->p[0:s->n_beads])
    #pragma acc update host(s->im[0:s->n_beads])
    #pragma acc update host(s->bl_len[0:s->n_beads])
    #pragma acc update host(s->bl[0:s->n_beads*s->bl_len_max])
    #pragma acc update host(s->type[0:s->n_beads])
    #pragma acc update host(s->id_orig[0:s->n_beads])

    hsize_t d1[1] = {1}, d3[1] = {3}, dm1[1] = {s->n_beads}, dm3[2] = {s->n_beads, 3}, dbl[2] = {s->n_beads, s->bl_len_max}, dt2[2] = {s->n_types, s->n_types}, dt1[1] = {s->n_types}, dn[1] = {s->n_nanos};

    write_dataset(data_id, s->r, "r", 2, dm3, H5T_NATIVE_REAL);
    write_dataset(data_id, s->im, "im", 2, dm3, H5T_NATIVE_INT);
    write_dataset(data_id, s->p, "p", 2, dm3, H5T_NATIVE_REAL);
    write_dataset(data_id, s->type, "type", 1, dm1, H5T_NATIVE_INT);
    write_dataset(data_id, s->id_orig, "id_orig", 1, dm1, H5T_NATIVE_INT);
    write_dataset(data_id, s->gamma, "gamma", 2, dt2, H5T_NATIVE_REAL);
    write_dataset(data_id, s->rc, "rc", 2, dt2, H5T_NATIVE_REAL);
    write_dataset(data_id, s->a, "a", 2, dt2, H5T_NATIVE_REAL);
    write_dataset(data_id, s->epsilon_lj, "epsilon_lj", 2, dt2, H5T_NATIVE_REAL);
    write_dataset(data_id, s->sigma_lj, "sigma_lj", 2, dt2, H5T_NATIVE_REAL);
    write_dataset(data_id, s->kb, "kb", 2, dt2, H5T_NATIVE_REAL);
    write_dataset(data_id, s->r0, "r0", 2, dt2, H5T_NATIVE_REAL);
    write_dataset(data_id, s->bl, "bl", 2, dbl, H5T_NATIVE_INT);
    write_dataset(data_id, s->bl_len, "bl_len", 1, dm1, H5T_NATIVE_INT);
    write_dataset(data_id, s->nano_list, "nano_list", 1, dn, H5T_NATIVE_INT);
    write_dataset(data_id, s->epsilon_wall_bot, "epsilon_wall_bot", 1, dt1, H5T_NATIVE_REAL);
    write_dataset(data_id, s->epsilon_wall_top, "epsilon_wall_top", 1, dt1, H5T_NATIVE_REAL);
    write_dataset(data_id, &s->wall_thickness, "wall_thickness", 1, d1, H5T_NATIVE_REAL);
    write_dataset(data_id, &s->b, "b", 1, d1, H5T_NATIVE_REAL);
    write_dataset(data_id, &s->k_bend, "k_bend", 1, d1, H5T_NATIVE_REAL);
    write_dataset(data_id, &s->sigma_wall, "sigma_wall", 1, d1, H5T_NATIVE_REAL);
    write_dataset(data_id, &s->rd, "rd", 1, d1, H5T_NATIVE_REAL);
    write_dataset(data_id, &s->n_types, "n_types", 1, d1, H5T_NATIVE_INT);
    write_dataset(data_id, &s->n_types_non_evaporating, "n_types_non_evaporating", 1, d1, H5T_NATIVE_INT);
    write_dataset(data_id, &s->n_beads, "n_beads", 1, d1, H5T_NATIVE_INT);
    write_dataset(data_id, &s->n_nanos, "n_nanos", 1, d1, H5T_NATIVE_INT);
    write_dataset(data_id, &s->bl_len_max, "bl_len_max", 1, d1, H5T_NATIVE_INT);
    write_dataset(data_id, s->L, "L", 1, d3, H5T_NATIVE_REAL);
    write_dataset(data_id, &s->time, "time", 1, d1, H5T_NATIVE_DOUBLE);
    write_dataset(data_id, &s->kT, "kT", 1, d1, H5T_NATIVE_REAL);

    if(!is_restarting)
    {
        H5Gclose(group_id);
    }

    H5Fclose(file_id);
}

void write_dataset(hid_t file_id, void *data, const char *name, int rank, hsize_t * current_dims, hid_t dtype_id)
{
    hid_t dataspace_id = H5Screate_simple(rank, current_dims, NULL);
    hid_t dataset_id = H5Dcreate2(file_id, name, dtype_id, dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    H5Dwrite(dataset_id, dtype_id, H5S_ALL, H5S_ALL, H5P_DEFAULT, data);
    H5Dclose(dataset_id);
    H5Sclose(dataspace_id);
}

void read_dataset(hid_t file_id, void *data, const char *name, hid_t dtype_id)
{
    hid_t dataset_id = H5Dopen2(file_id, name, H5P_DEFAULT);
    H5Dread(dataset_id, dtype_id, H5S_ALL, H5S_ALL, H5P_DEFAULT, data);
    H5Dclose(dataset_id);
}

void write_simulation_data(MySystem * s)
{
    int t = s->time_step;

    if(t % s->conf_save_dt_int == 0 && s->conf_save_dt_int > 0)
    {
        save_conf(s, 0);
        save_conf(s, 1);
    }

    if(t % s->ana_dt_int == 0)
    {
        ana(s);
    }

    if(t % s->sample_fields_dt_int == 0)
    {
        ana_fields(s);
    }
}

int print_info(MySystem * s, struct timespec time_ref)
{
    struct timespec time_current;
    static struct timespec time_last = {0, 0};
    static int t_last = 0;
    real elapsed_time, tps;

    clock_gettime(CLOCK_REALTIME, &time_current);
    time_current.tv_sec -= time_ref.tv_sec;
    time_current.tv_nsec -= time_ref.tv_nsec;
    elapsed_time = time_current.tv_sec - time_last.tv_sec + (time_current.tv_nsec - time_last.tv_nsec) / 1e9;
    if(elapsed_time > s->info_dt)
    {
        tps = (s->time_step - t_last) * s->dt / elapsed_time;
        printf("INFO: Running for %d s | Simulation time: %.12g (%.3g %%) | TPS = %.3g\n", (int) (time_current.tv_sec), s->time, (real) s->time_step / s->sim_time_int * 100, tps);
        t_last = s->time_step;
        time_last.tv_sec = time_current.tv_sec;
        time_last.tv_nsec = time_current.tv_nsec;
    }
    return (int)time_current.tv_sec;
}

// due to reallocating of the neighbor_list (because the maximum number of neighbors depends on the physics/dynamics),
// the whole main and gpu memory gets reallocated to avoid fragmentation and therefore memory issues (e.g. being out of memory)
int defragment_memory(MySystem * s)
{
    if(s->defragmentation_needed || (s->defragment_memory_dt_int > 0 && s->time_step % s->defragment_memory_dt_int == 0))
    {
        printf("INFO: Defragment CPU and GPU memory because nl was reallocated (nl_len_max = %d)\n", s->nl_len_max);
        // save current state of MySystem on disk and stack
        const MySystem tmp = *s;
        save_conf(s, 1);
        char fn[1024];
        sprintf(fn, "%s_conf_restart.h5", s->prefix);
        finalize_MySystem(s);

        // load again current state of MySystem from disk and stack; But now it should be defragmented in CPU and GPU memory
        *s = tmp;
        s->conf_file_name = fn;
        initialize_MySystem(s);
        s->time_0 = tmp.time_0;

        s->defragmentation_needed = 0;
    }

    return 0;
}
