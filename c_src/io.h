#include <signal.h>
#include <time.h>
#include <hdf5.h>

extern volatile int NoKeyboardInterrupt;
void intHandler(int dummy);

int initialize_MySystem(MySystem * s);
void finalize_MySystem(MySystem * s);
void init_rest(MySystem * s);
void read_conf_scalars(MySystem * s);
void read_conf_arrays(MySystem * s);
void read_dataset(hid_t file_id, void *data, const char *name, hid_t dtype_id);
void write_dataset(hid_t file_id, void *data, const char *name, int rank, hsize_t * current_dims, hid_t dtype_id);
void save_conf(MySystem * s, int is_restarting);
void save_ana(MySystem * s);
void write_simulation_data(MySystem * s);
int print_info(MySystem * s, struct timespec time_ref);
int defragment_memory(MySystem * s);
void check_reallocation_of_id_orig_rev(MySystem * s);
