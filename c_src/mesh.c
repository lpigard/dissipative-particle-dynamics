// calculating the mesh of cells that are used to calculate the neighborlist efficiently

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "system.h"
#include "mesh.h"

void init_mesh(MySystem *s)
{
	real r_nl = s->rd;

	for(int i = 0; i < s->n_types; i++)
	{
		for(int j = 0; j < s->n_types; j++)
		{
			const real r = s->rc[i * s->n_types + j];

			if(r > r_nl)
			{
				r_nl = r;
			}
		}
	}

	r_nl += sqrt(s->r_buffer2);
	s->r_nl2 = r_nl * r_nl;

    s->n_nl_rebuild = 0;

	for(int d = 0; d < 3; d++)
	{
		s->n[d] = (int)(s->L[d] / r_nl);
	}

	if(s->integration_scheme > 0)
	{
		for(int d = 0; d < 3; d++)
		{
			while(s->n[d] % 3 != 0)
			{
				s->n[d]--;
			}
		}
	}

	if(s->mc_dt_int > 0)
	{
		for(int d = 0; d < 3; d++)
		{
			while(s->n[d] % 2 != 0)
			{
				s->n[d]--;
			}
		}
	}

	for(int d = 0; d < 3; d++)
	{
		s->l[d] = s->L[d] / s->n[d];
	}

    const int n_cells = s->n_cells = s->n[0] * s->n[1] * s->n[2];

	s->cell_begin = malloc(n_cells * sizeof *s->cell_begin);
	s->cell_end = malloc(n_cells * sizeof *s->cell_end);
	s->cell_neighbor = malloc(n_cells * sizeof *s->cell_neighbor);
	s->density_z = malloc(s->n[2] * sizeof *s->density_z);
	calc_cell_neighbor(s);
}

void finalize_mesh(MySystem *s)
{
	#pragma acc exit data delete(s->cell_begin[0:s->n_cells])
    #pragma acc exit data delete(s->cell_end[0:s->n_cells])
	#pragma acc exit data delete(s->cell_neighbor[0:s->n_cells])
	#pragma acc exit data delete(s->density_z[0:s->n[2]])

	free(s->cell_begin);
    free(s->cell_end);
	free(s->cell_neighbor);
	free(s->density_z);
}

real wrap_back_dis(const real d, const real l)
{
    return d - l * nearbyint(d / l);
}

int id_cell(MySystem const * const s, const int i)
{
	int r[3];

	for(int d = 0; d < 3; d++)
	{
		r[d] = floor(s->r[i][d] / s->l[d]);
	}

    return r[0] + s->n[0] * (r[1] + s->n[1] * r[2]);
}

void calc_cell_neighbor(MySystem *s)
{
    for(int x = 0; x < s->n[0]; x++)
    {
        for(int y = 0; y < s->n[1]; y++)
        {
            for(int z = 0; z < s->n[2]; z++)
            {
                int c = x + s->n[0] * (y + s->n[1] * z);
                int n = 0;
                for(int dx = -1; dx <= 1; dx++)
                {
                    for(int dy = -1; dy <= 1; dy++)
                    {
                        for(int dz = -1; dz <= 1; dz++)
                        {
                            int xn = x + dx;
                            int yn = y + dy;
                            int zn = z + dz;

                            if(xn < 0)
                            {
                                xn += s->n[0];
                            }
                            else if(xn >= s->n[0])
                            {
                                xn -= s->n[0];
                            }

                            if(yn < 0)
                            {
                                yn += s->n[1];
                            }
                            else if(yn >= s->n[1])
                            {
                                yn -= s->n[1];
                            }

                            if(zn < 0)
                            {
                                zn += s->n[2];
                            }
                            else if(zn >= s->n[2])
                            {
                                zn -= s->n[2];
                            }

                            int cn = xn + s->n[0] * (yn + s->n[1] * zn);
                            s->cell_neighbor[c][n] = cn;
                            n++;
                        }
                    }
                }
            }
        }
    }
}
