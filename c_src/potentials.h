
#pragma acc routine seq
real bond_potential(const real r, const real k, const real r0);
#pragma acc routine seq
real bond_force(const real r, const real k, const real r0);
#pragma acc routine seq
real dpd_a_potential(const real r, const real a, const real rc);
#pragma acc routine seq
real dpd_a_force(const real r, const real a, const real rc);
void calc_rho(MySystem * s);
#pragma acc routine seq
real dpd_b_potential(const real b, const real rd, const real rho);
#pragma acc routine seq
real dpd_b_force(const real r, const real b, const real rd, const real rho_i, const real rho_j);
#pragma acc routine seq
real lj_potential(const real r, const real epsilon, const real sigma);
#pragma acc routine seq
real lj_force(const real r, const real epsilon, const real sigma);
#pragma acc routine seq
real wall_potential(const real z, const real epsilon, const real sigma);
#pragma acc routine seq
real wall_force(const real z, const real epsilon, const real sigma);
// #pragma acc routine seq
// real top_wall_potential(const real z, const real epsilon, const real sigma);
// #pragma acc routine seq
// real top_wall_force(const real z, const real epsilon, const real sigma);
