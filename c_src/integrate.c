// handling the integration of the equations of motion via velocity-verlet algorithm. Thermostat is either DPD (default) or peters thermostat.

#include "system.h"
#include "integrate.h"
#include "saru.h"
#include "mesh.h"
#include "potentials.h"
#include "sort.h"
#include "evaporation.h"

void sort(int *d_key, int *d_values, int num, void *stream);

void integrate(MySystem * s)
{
    vv_step_one(s);
    vv_step_two(s);
    if(s->integration_scheme > 0 && s->time_step % s->peters_dt_int == 0)
    {
        peters_thermostat(s);
    }
    if(s->mc_dt_int > 0 && s->time_step % s->mc_dt_int == 0 && !s->wl_flag)
    {
        mc(s);
    }
    healing(s);
    s->time_step++;
    #pragma acc update device(s->time_step)
    s->time = s->time_0 + s->dt_dbl * s->time_step;
}

void vv_step_one(MySystem * s)
{
    real max_dr2 = 0;

    #pragma acc parallel loop present(s[0:1]) reduction(max:max_dr2)
    for(int i = 0; i < s->n_beads; i++)
    {
        real dr2 = 0;

        #pragma acc loop seq
        for(int d = 0; d < 3; d++)
        {
            s->p[i][d] += s->f[i][d] * (real)0.5 * s->dt;
            s->r[i][d] += s->p[i][d] * s->dt;
            const int dim = (int)floor(s->r[i][d] / s->L[d]);
            s->im[i][d] += dim;
            s->r[i][d] -= dim * s->L[d];
            const real dr = s->r[i][d] - s->r_check[i][d] + (s->im[i][d] - s->im_check[i][d]) * s->L[d];
            dr2 += dr * dr;
        }

        if(dr2 > max_dr2)
        {
            max_dr2 = dr2;
        }
    }

    int recalculate_nl = 0;

    if(s->time_step % s->reinsert_bulk_dt_int == 0 && s->h_crit >= 0. && s->reinsert_bulk)
    {
        if(s->time_step / s->reinsert_bulk_dt_int > 0)
        {
            calc_h_crit(s);
        }

        if(s->h < s->h_crit)
        {
            reinsert_bulk(s);
            recalculate_nl++;
        }
    }

    if(s->time_step % s->vapor_zone_dt_int == 0 && s->rho_vapor_zone >= 0.)
    {
        recalculate_nl += abs(set_vapor_density(s));
    }

    recalculate_nl += max_dr2 > (real)0.25 * s->r_buffer2;

    if(recalculate_nl)
    {
        calc_nl(s);
        calc_forces(s);
    }
}

void vv_step_two(MySystem * s)
{
    calc_forces(s);

    #pragma acc parallel loop present(s[0:1]) collapse(2)
    for(int i = 0; i < s->n_beads; i++)
    {
        for(int d = 0; d < 3; d++)
        {
            s->p[i][d] += (real)0.5 * s->f[i][d] * s->dt;
        }
    }
}

void calc_nl(MySystem * s)
{
    sort_system(s);

    #pragma acc parallel loop present(s[0:1])
    for(int i = 0; i < s->n_beads; i++)
    {
        const int c = s->id_cell[i];

        int n = 0;

        #pragma acc loop seq
        for(int cn0 = 0; cn0 < 27; cn0++)
        {
            const int cn = s->cell_neighbor[c][cn0];

            #pragma acc loop seq
            for(int j = s->cell_begin[cn]; j < s->cell_end[cn]; j++)
            {
                real r2 = 0;

                #pragma acc loop seq
                for(int d = 0; d < 3; d++)
                {
                    const real dr = wrap_back_dis(s->r[i][d] - s->r[j][d], s->L[d]);
                    r2 += dr * dr;
                }

                if(r2 < s->r_nl2 && r2 > EPSILON * EPSILON)
                {
                    n++;
                }
            }
        }

        s->nl_len[i] = n;
    }

    int nl_len_max = 0;

    #pragma acc parallel loop present(s[0:1]) reduction(max:nl_len_max)
    for(int i = 0; i < s->n_beads; i++)
    {
        const int n = s->nl_len[i];

        if(n > nl_len_max)
        {
            nl_len_max = n;
        }
    }

    if(nl_len_max > s->nl_len_max)
    {
        nl_len_max = (int)(nl_len_max * 1.05); // allocate ~5% more memory to avoid too many reallocations/defragmentations
        #pragma acc exit data delete(s->nl[0:s->n_beads_memory*s->nl_len_max])
        s->nl_len_max = nl_len_max;
        #pragma acc update device(s->nl_len_max)
        free(s->nl);
        s->nl = malloc(s->n_beads_memory * s->nl_len_max * sizeof *s->nl);
        #pragma acc enter data create(s->nl[0:s->n_beads_memory*s->nl_len_max])
        s->defragmentation_needed = 1;
    }

    #pragma acc parallel loop present(s[0:1])
    for(int i = 0; i < s->n_beads; i++)
    {
        const int c = s->id_cell[i];

        int n = 0;

        #pragma acc loop seq
        for(int cn0 = 0; cn0 < 27; cn0++)
        {
            const int cn = s->cell_neighbor[c][cn0];

            #pragma acc loop seq
            for(int j = s->cell_begin[cn]; j < s->cell_end[cn]; j++)
            {
                real r2 = 0;

                #pragma acc loop seq
                for(int d = 0; d < 3; d++)
                {
                    const real dr = wrap_back_dis(s->r[i][d] - s->r[j][d], s->L[d]);
                    r2 += dr * dr;
                }

                if(r2 < s->r_nl2 && r2 > EPSILON)
                {
                    s->nl[i * s->nl_len_max + n] = j;
                    n++;
                }
            }
        }
    }

    s->n_nl_rebuild++;
}

void calc_forces(MySystem * s)
{
    calc_rho(s);

    #pragma acc parallel loop present(s[0:1])
    for(int i = 0; i < s->n_beads; i++)
    {
        #pragma acc loop seq
        for(int d = 0; d < 3; d++)
        {
            s->f[i][d] = 0.;
        }
    }

    #pragma acc parallel loop present(s[0:1])
    for(int i = 0; i < s->n_beads; i++)
    {
        real sum[3] = {0};

        #pragma acc loop seq
        for(int j0 = 0; j0 < s->bl_len[i]; j0++)
        {
            const int j = s->bl[i * s->bl_len_max + j0];

            real dr[3];
            real r = 0;

            #pragma acc loop seq
            for(int d = 0; d < 3; d++)
            {
                dr[d] = wrap_back_dis(s->r[i][d] - s->r[j][d], s->L[d]);
                r += dr[d] * dr[d];
            }

            r = sqrtf(r);

            if(r > EPSILON)
            {
                const int mindex = s->type[i] * s->n_types + s->type[j];
                const real kb = s->kb[mindex];
                const real r0 = s->r0[mindex];
                const real f_div_r = bond_force(r, kb, r0) / r;

                #pragma acc loop seq
                for(int d = 0; d < 3; d++)
                {
                    sum[d] += f_div_r * dr[d];
                }
            }
        }

        #pragma acc loop seq
        for(int j0 = 0; j0 < s->nl_len[i]; j0++)
        {
            const int j = s->nl[i * s->nl_len_max + j0];
            real dr[3];
            real r = 0;

            #pragma acc loop seq
            for(int d = 0; d < 3; d++)
            {
                dr[d] = wrap_back_dis(s->r[i][d] - s->r[j][d], s->L[d]);
                r += dr[d] * dr[d];
            }

            r = sqrtf(r);

            if(r > EPSILON)
            {
                real f = (real)0;
                const int mindex = s->type[i] * s->n_types + s->type[j];
                const real rc = s->rc[mindex];

                if(r < rc)
                {
                    const int mindex = s->type[i] * s->n_types + s->type[j];
                    const real a = s->a[mindex];
                    const real w = (real)1 - r / rc;
                    f += a * w;

                    //if(s->integration_scheme == 0) // comment out for now for performance reasons; uncomment if peter scheme is needed
                    //{
                    const real gamma = s->gamma[mindex];
                    const real xi = saru_normal_simple(i, j, s->time_step);

                    real r_dot_p = 0;

                    #pragma acc loop seq
                    for(int d = 0; d < 3; d++)
                    {
                        r_dot_p += dr[d] * (s->p[i][d] - s->p[j][d]);
                    }

                    r_dot_p /= r;

                    f += (- gamma * w * r_dot_p + sqrtf((real)2 * s->kT * gamma / s->dt) * xi) * w;
                    //}
                }

                const real rd = s->rd;

                if(r < rd)
                {
                    f += dpd_b_force(r, s->b, rd, s->rho[i], s->rho[j]);
                }

                f /= r;

                #pragma acc loop seq
                for(int d = 0; d < 3; d++)
                {
                    sum[d] += f * dr[d];
                }
            }
        }

        #pragma acc loop seq
        for(int j0 = 0; j0 < s->n_nanos; j0++)
        {
            const int j = s->id_orig_rev[s->nano_list[j0]];

            real dr[3];
            real r = 0;

            #pragma acc loop seq
            for(int d = 0; d < 3; d++)
            {
                dr[d] = wrap_back_dis(s->r[i][d] - s->r[j][d], s->L[d]);
                r += dr[d] * dr[d];
            }

            r = sqrtf(r);

            if(r > EPSILON)
            {
                const int mindex = s->type[i] * s->n_types + s->type[j];
                const real sigma = s->sigma_lj[mindex];

                if(r < 2.5 * sigma)
                {
                    const real eps = s->epsilon_lj[mindex];
                    const real f_div_r = lj_force(r, eps, sigma) / r;

                    #pragma acc loop seq
                    for(int d = 0; d < 3; d++)
                    {
                        sum[d] += f_div_r * dr[d];
                        #pragma acc atomic
                        s->f[j][d] -= f_div_r * dr[d];
                    }
                }
            }
        }

        if(s->bl_len[i] == 2 && s->k_bend > EPSILON)
        {
            const int j = s->bl[i * s->bl_len_max + 0];

            real dr_ij[3];
            real r_ij = 0;

            #pragma acc loop seq
            for(int d = 0; d < 3; d++)
            {
                dr_ij[d] = wrap_back_dis(s->r[j][d] - s->r[i][d], s->L[d]);
                r_ij += dr_ij[d] * dr_ij[d];
            }

            r_ij = sqrtf(r_ij);

            const int k = s->bl[i * s->bl_len_max + 1];

            real dr_ik[3];
            real r_ik = 0;

            #pragma acc loop seq
            for(int d = 0; d < 3; d++)
            {
                dr_ik[d] = wrap_back_dis(s->r[k][d] - s->r[i][d], s->L[d]);
                r_ik += dr_ik[d] * dr_ik[d];
            }

            r_ik = sqrtf(r_ik);

            const real prefactor = s->k_bend / (r_ij * r_ik);
            real r_dot_r = 0.;

            #pragma acc loop seq
            for(int d = 0; d < 3; d++)
            {
                r_dot_r += dr_ij[d] * dr_ik[d];
            }

            #pragma acc loop seq
            for(int d = 0; d < 3; d++)
            {
                const real fj = prefactor * (-dr_ik[d] + r_dot_r * dr_ij[d] / (r_ij * r_ij));
                const real fk = prefactor * (-dr_ij[d] + r_dot_r * dr_ik[d] / (r_ik * r_ik));

                #pragma acc atomic
                s->f[i][d] -= fj + fk;
                #pragma acc atomic
                s->f[j][d] += fj;
                #pragma acc atomic
                s->f[k][d] += fk;
            }
        }

        const real sigma = s->sigma_wall;

        real dz = s->r[i][2] - (real)0.5 * s->wall_thickness;
        sum[2] += wall_force(dz, s->epsilon_wall_bot[s->type[i]], sigma);

        dz = s->L[2] - (real)0.5 * s->wall_thickness - s->r[i][2];
        sum[2] -= wall_force(dz, s->epsilon_wall_top[s->type[i]], sigma);

        sum[2] -= s->gravitational_constant * (1 + 10. * s->p[i][2]);

        #pragma acc loop seq
        for(int d = 0; d < 3; d++)
        {
            #pragma acc atomic
            s->f[i][d] += sum[d];
        }
    }
}

void peters_thermostat(MySystem * s)
{
    const real dt = s->dt * s->peters_dt_int;

    for(int x0 = 0; x0 < 3; x0++)
    {
        for(int y0 = 0; y0 < 3; y0++)
        {
            for(int z0 = 0; z0 < 3; z0++)
            {
                const int c0 = x0 + s->n[0] * (y0 + s->n[1] * z0);

                #pragma acc parallel loop present(s[0:1]) collapse(3)
                for(int dx = 0; dx < s->n[0]; dx += 3)
                {
                    for(int dy = 0; dy < s->n[1]; dy += 3)
                    {
                        for(int dz = 0; dz < s->n[2]; dz += 3)
                        {
                            const int c = c0 + dx + s->n[0] * (dy + s->n[1] * dz);
                            #pragma acc loop seq
                            for(int i = s->cell_begin[c]; i < s->cell_end[c]; i++)
                            {
                                #pragma acc loop seq
                                for(int j0 = 0; j0 < s->nl_len[i]; j0++)
                                {
                                    const int j = s->nl[i * s->nl_len_max + j0];
                                    if(j > i)
                                    {
                                        real dr[3];
                                        real r = 0;

                                        #pragma acc loop seq
                                        for(int d = 0; d < 3; d++)
                                        {
                                            dr[d] = wrap_back_dis(s->r[i][d] - s->r[j][d], s->L[d]);
                                            r += dr[d] * dr[d];
                                        }

                                        r = sqrtf(r);

                                        if(r > EPSILON)
                                        {
                                            const int mindex = s->type[i] * s->n_types + s->type[j];
                                            const real rc = s->rc[mindex];

                                            if(r < rc)
                                            {
                                                real w = 1 - r / rc;
                                                w *= w;
                                                const int mindex = s->type[i] * s->n_types + s->type[j];
                                                real a = s->gamma[mindex] * w;
                                                real b;
                                                if(s->integration_scheme == 1)
                                                {
                                                    b = sqrtf((real)2. * s->kT * a * dt * ((real)1. - a * dt));

                                                }
                                                else
                                                {
                                                    b = (real)0.5 * s->kT * ((real)1. - exp((real)-4. * a * dt));
                                                    a = ((real)1. - exp((real)-2. * a * dt)) / ((real)2. * dt);
                                                }

                                                const real xi = saru_normal_simple(i, j, s->time_step);
                                                real r_dot_p = 0;

                                                #pragma acc loop seq
                                                for(int d = 0; d < 3; d++)
                                                {
                                                    r_dot_p += dr[d] * (s->p[i][d] - s->p[j][d]);
                                                }

                                                r_dot_p /= r;
                                                const real dp = (-a * r_dot_p * dt + b * xi) / r;

                                                for(int d = 0; d < 3; d++)
                                                {
                                                    s->p[i][d] += dp * dr[d];
                                                    s->p[j][d] -= dp * dr[d];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

void mc(MySystem * s)
{
    const int type_alpha = 0;
    const int type_beta = s->n_types - 1;
    int n_accepts = 0;
    int n_mc = 0;
    const real h0 = s->h0_start + (s->h0_end - s->h0_start) / s->sim_time_int * s->time_step;

    for(int x00 = 0; x00 < 2; x00++)
    {
        for(int y00 = 0; y00 < 2; y00++)
        {
            for(int z00 = 0; z00 < 2; z00++)
            {
                int x0 = saru_uniform_int_simple(2, x00, 3 * s->n_beads, s->time_step);
                x0 = x0 % 2;
                int y0 = saru_uniform_int_simple(2, y00, 3 * s->n_beads, s->time_step);
                y0 = y0 % 2;
                int z0 = saru_uniform_int_simple(2, z00, 3 * s->n_beads, s->time_step);
                z0 = z0 % 2;
                const int c0 = x0 + s->n[0] * (y0 + s->n[1] * z0);

                #pragma acc parallel loop present(s[0:1]) collapse(3) reduction(+:n_accepts,n_mc)
                for(int dx = 0; dx < s->n[0]; dx += 2)
                {
                    for(int dy = 0; dy < s->n[1]; dy += 2)
                    {
                        for(int dz = 0; dz < s->n[2]; dz += 2)
                        {
                            const int c = c0 + dx + s->n[0] * (dy + s->n[1] * dz);
                            #pragma acc loop seq
                            for(int i0 = s->cell_begin[c]; i0 < s->cell_end[c]; i0++)
                            // const int i0 = 0;
                            {
                                // pick a random bead within the cell
                                int i = s->cell_begin[c] + saru_uniform_int_simple(s->cell_end[c] - s->cell_begin[c], i0, 2 * s->n_beads, s->time_step);
                                i = i % s->n_beads;

                                const int type_old = s->type[i];
                                if(type_old == type_alpha || type_old == type_beta)
                                {
                                    const real energy_old = energy(i, s);
                                    real h;

                                    int type_new;
                                    if(type_old == type_alpha)
                                    {
                                        type_new = type_beta;
                                        h = h0;
                                    }
                                    else
                                    {
                                        type_new = type_alpha;
                                        h = -h0;
                                    }

                                    s->type[i] = type_new;
                                    const real energy_new = energy(i, s);

                                    Saru state;
                                    saru_init(&state, i, s->n_beads, s->time_step);
                                    const real p = saru_uniform(&state);
                                    const real p_acc = exp(-(energy_new - energy_old + h) / s->kT);

                                    if ((p_acc > 1) || (p_acc > p))
                                    {
                                        n_accepts++;
                                    }
                                    else
                                    {
                                        s->type[i] = type_old;
                                    }

                                    n_mc++;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    s->n_accepts += n_accepts;
    s->n_mc += n_mc;
}

real energy(const int i, MySystem * s)
{
    real energy = 0.;

    #pragma acc loop seq
    for(int j0 = 0; j0 < s->nl_len[i]; j0++)
    {
        int j = s->nl[i * s->nl_len_max + j0];
        real r = 0;

        #pragma acc loop seq
        for(int d = 0; d < 3; d++)
        {
            const real dr = wrap_back_dis(s->r[i][d] - s->r[j][d], s->L[d]);
            r += dr * dr;
        }

        r = sqrtf(r);

        if(r > EPSILON)
        {
            const int mindex = s->type[i] * s->n_types + s->type[j];

            const real rc = s->rc[mindex];

            if(r < rc)
            {
                const real a = s->a[mindex];
                energy += dpd_a_potential(r, a, rc);
            }
        }
    }

    return energy;
}
