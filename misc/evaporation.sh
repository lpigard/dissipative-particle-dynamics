#!/bin/bash -x
#SBATCH --account=psm
#SBATCH --nodes=1
#SBATCH --output=out.%j
#SBATCH --error=err.%j
#SBATCH --time=2:00:00
#SBATCH --partition=develbooster
#SBATCH --gres=gpu:4

icpu=( 18 6 40 30 )
rhov=( 0 0.0001 0.001 0.01 )

for j in {0..3}
do
	name=test
	name_eva=test_rhov${rhov[${j}]}
	cp ${name}_conf_restart.h5 ${name_eva}_conf_restart.h5
	srun --exclusive -n 1 --gres=gpu:1 --cpu-bind=map_cpu:${icpu[${j}]} ./DPD ${name_eva} ${name_eva}_conf_restart.h5 ${name_eva}_ana.dat 20000 0.01 -1 10 ${rhov[${j}]} 0 -1 >> ${name_eva}.log &
done

wait
