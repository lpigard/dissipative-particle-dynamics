#!/bin/bash -x
#SBATCH --account=psm
#SBATCH --nodes=1
#SBATCH --output=out.%j
#SBATCH --error=err.%j
#SBATCH --time=2:00:00
#SBATCH --partition=develbooster
#SBATCH --gres=gpu:4

g=( 0.1 0.05 0.025 0.01 0.005 0.0025 0.001 0.0005 0.00025 0.0001 0.00005 0.000025 0.00001 0 0 )
dt=( 0.001 0.01 0.01 0.01 0.01 0.01 0.01 0.01 0.01 0.01 0.01 0.01 0.01 0.01 0.01 )
t=( 100 100 200 300 300 1000 1000 1000 1000 1000 1000 1000 1000 1000 30000 )

name=test
python ConfGen.py -o ${name}

for i in {0..13}
do
	name=test
	srun --exclusive -n 1 --gres=gpu:1 --cpu-bind=map_cpu:0 ./DPD ${name} ${name}_conf_restart.h5 ${name}_ana.dat ${t[${i}]} ${dt[${i}]} -1 -1 -1 ${g[${i}]} -1 >> ${name}_init.log &

	wait
done
