import numpy as np
import h5py
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-o',
            action="store", dest="name", required = True,
            help="name for output file")

arguments = parser.parse_args()

n_types = 2
n_types_non_evaporating = 0

type_H2O = 0
type_HCl = 1

M1 = np.zeros((n_types, n_types)) + 1.

a = -np.array([ [15,    17.5],
                [17.5,  20]     ])

rc = 1.
b = 30
rd = 0.65
kT = 1.

kb = 6. * M1
r0 = 0. * M1
gamma = 0.5 * M1

h = 20
L = np.array([10, 10, 2 * h])
A = L[0] * L[1]
N = 32

rhol = 4
rhov = 0.06

n_beads = int(A * (h * rhol + (L[2] - h) * rhov))
n_polys = 0
n_solvent = n_beads - n_polys * N
print(n_beads)

wall_thickness = 1.
sigma_wall = 2.
z0 = wall_thickness / 2 + (2. / 5.)**(1. / 6.) * sigma_wall
epsilon_wall_bot = np.repeat(0.1, n_types)
epsilon_wall_top = np.repeat(0.1, n_types)

r = np.zeros((n_beads, 3))

offset = 0
print('calc solvent positions')

for j in range(n_solvent):
    i = j + offset

    r[i, 0] = np.random.uniform(0, L[0], 1)
    r[i, 1] = np.random.uniform(0, L[1], 1)
    r[i, 2] = np.random.uniform(z0, h + z0, 1)

im = np.zeros((n_beads, 3), dtype=int)

dimx = np.floor(r[:, 0] / L[0]).astype(int)
dimy = np.floor(r[:, 1] / L[1]).astype(int)
dimz = np.floor(r[:, 2] / L[2]).astype(int)

im[:, 0] += dimx
im[:, 1] += dimy
im[:, 2] += dimz

r[:, 0] -= dimx * L[0]
r[:, 1] -= dimy * L[1]
r[:, 2] -= dimz * L[2]

p = np.zeros((n_beads, 3))

bl_len = np.zeros(n_beads, dtype = int)
bl_len_max = 2
offset = 0
for p in range(n_polys):
    for m in range(N):
        i = p * N + m + offset
        if m == 0 or m == N - 1:
            bl_len[i] = 1
        else:
            bl_len[i] = 2


bl = np.zeros((n_beads, bl_len_max))

for p in range(n_polys):
    for m in range(N):
        i = p * N + m + offset
        if m == 0:
            bl[i, 0] = i + 1
        elif m == N - 1:
            bl[i, 0] = i - 1
        else:
            bl[i, 0] = i - 1
            bl[i, 1] = i + 1

phiA = 0.5

type = np.zeros(n_beads)
type[int(phiA * n_beads):] = 1

poly_offset = np.array(range(n_polys)) * N
poly_N = np.zeros(n_polys) + N

rho_HCL_thresh = 0.
rho_H2O_thresh = 0.

id_orig = np.array(range(n_beads))

type_nanoparticles = -1;
type_polymer_B = -1
time = 0.

f = h5py.File(arguments.name + '_conf_restart.h5', 'w')
f.create_dataset("r", dtype=float, data = r)
f.create_dataset("im", dtype=np.intc, data = im)
f.create_dataset("type", dtype=np.intc, data = type)
f.create_dataset("p", dtype=float, data = p)
f.create_dataset("bl", dtype=np.intc, data = bl)
f.create_dataset("bl_len", dtype=np.intc, data = bl_len)
f.create_dataset("poly_offset", dtype=np.intc, data = poly_offset)
f.create_dataset("rc", dtype=float, data = rc)
f.create_dataset("rd", dtype=float, data = rd)
f.create_dataset("gamma", dtype=float, data = gamma)
f.create_dataset("a", dtype=float, data = a)
f.create_dataset("b", dtype=float, data = b)
f.create_dataset("epsilon_wall_bot", dtype=float, data = epsilon_wall_bot)
f.create_dataset("epsilon_wall_top", dtype=float, data = epsilon_wall_top)
f.create_dataset("sigma_wall", dtype=float, data = sigma_wall)
f.create_dataset("r0", dtype=float, data = r0)
f.create_dataset("kb", dtype=float, data = kb)
f.create_dataset("poly_N", dtype=np.intc, data = poly_N)
f.create_dataset("n_types", dtype=np.intc, data = n_types)
f.create_dataset("n_types_non_evaporating", dtype=np.intc, data = n_types_non_evaporating)
f.create_dataset("n_beads", dtype=np.intc, data = n_beads)
f.create_dataset("n_polys", dtype=np.intc, data = n_polys)
f.create_dataset("bl_len_max", dtype=np.intc, data = bl_len_max)
f.create_dataset("L", dtype=float, data = L)
f.create_dataset("kT", dtype=float, data = kT)
f.create_dataset("time", dtype=float, data = time)
f.create_dataset("wall_thickness", dtype=float, data = wall_thickness)
f.create_dataset("rho_HCL_thresh", dtype=float, data = rho_HCL_thresh)
f.create_dataset("rho_H2O_thresh", dtype=float, data = rho_H2O_thresh)
f.create_dataset("id_orig", dtype=np.intc, data = id_orig)
f.create_dataset("type_nanoparticles", dtype=np.intc, data = type_nanoparticles)
f.create_dataset("type_polymer_B", dtype=np.intc, data = type_polymer_B)
f.create_dataset("type_H2O", dtype=np.intc, data = type_H2O)
f.create_dataset("type_HCl", dtype=np.intc, data = type_HCl)
f.close()
